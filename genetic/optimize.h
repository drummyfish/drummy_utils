/*
  A function that performs a simple optimization/normalization of given program,
  i.e. makes it smaller/faster/simpler while preserving its behavior. If you
  change the instruction set this will likely seize to work correctly. Run the
  testing function below to check if this works.
*/

void programOptimize(Program program)
{
  while (1)
  {
    uint8_t opcode = *program;

    if (opcode == OPCODE_END)
      break;

    switch (opcode)
    {
      case OPCODE_MIN:
      case OPCODE_MAX:
      case OPCODE_ANB:
      case OPCODE_ORB:
        if (program[1] == program[2])
          program[0] = OPCODE_MOV;

        break;
      case OPCODE_EQU:
      case OPCODE_GRE:
      case OPCODE_NXL:
        if (program[1] == program[2])
        {
          program[0] = OPCODE_VAL;
          program[1] = 1;
        }

        break;

      case OPCODE_NEQ:
      case OPCODE_GRT:
      case OPCODE_XOB:
      case OPCODE_XOL:
      case OPCODE_MOD:
        if (program[1] == program[2])
        {
          program[0] = OPCODE_VAL;
          program[1] = 0;
        }

        break;

      case OPCODE_NXB:
        if (program[1] == program[2])
        {
          program[0] = OPCODE_VAL;
          program[1] = 255;
        }

        break;

      case OPCODE_NAB:
      case OPCODE_NOB:
        if (program[1] == program[2])
          program[0] = OPCODE_NEB;

        break;

      case OPCODE_ORL:
      case OPCODE_ANL:
      case OPCODE_DIV:
        if (program[1] == program[2])
          program[0] = OPCODE_NEZ;

        break;

      case OPCODE_MUX:
        if (program[1] == 0)
        {
          program[0] = OPCODE_MOV;
          program[1] = 0;
        }
        else if (program[1] == 255)
        {
          program[0] = OPCODE_VAL;
          program[1] = 1;
        }

        break;

      case OPCODE_ADX:
      case OPCODE_MXX:
        if (program[1] == 255)
        {
          program[0] = OPCODE_VAL;
          program[1] = 0;
        }
        else if (program[1] == 0)
        {
          program[0] = OPCODE_MOV;
          program[1] = 0;
        }

        break;

      case OPCODE_MIX:
        if (program[1] == 255)
        {
          program[0] = OPCODE_VAL;
          program[1] = 255;
        }
        else if (program[1] == 0)
        {
          program[0] = OPCODE_MOV;
          program[1] = 0;
        }

        break;

      case OPCODE_MI3:
      case OPCODE_MA3:
        if (program[1] == program[2])
        {
          if (program[2] == program[3])
            program[0] = OPCODE_MOV;
          else
          {
            program[0] = program[0] == OPCODE_MI3 ? OPCODE_MIN : OPCODE_MAX;
            program[2] = program[3];
          }
        }
        else if (program[2] == program[3])
        {
          if (program[1] == program[2])
            program[0] = OPCODE_MOV;
          else
            program[0] = program[0] == OPCODE_MI3 ? OPCODE_MIN : OPCODE_MAX;
        }
 
        break;

      default: break;
    }

    program += 4;
  }
}

int testOptimization(void)
{
  puts("testing optimization");

  randomSeed(0);

  for (int i = 0; i < 1000000; ++i)
  {
    Program p;
    Input in;
    Output out;
    Output out2;

    for (int j = 0; j < INPUT_BYTES; ++j)
      in[j] = random() % 256;

    programRandom(p);

    programExecute(p,in,out);

    programOptimize(p);

    programExecute(p,in,out2);
 
    for (int j = 0; j < OUTPUT_BYTES; ++j)
      if (out[j] != out2[j])
      {
        puts("ERROR");

        return 0;
      }
  }

  puts("OK");

  return 1;
}
