/* 
  Here you can modify different parameters of the simulation (see main.c for
  possible options).
*/

#define SEED 0
#define POPULATION_SIZE 100
#define POPULATION_ELITE 20
#define POPULATION_LOSERS 20
#define INPUT_BYTES 1
#define OUTPUT_BYTES 1
#define PROGRAM_MAX_LENGTH 30
