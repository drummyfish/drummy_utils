#!/bin/bash

clear; clear;

gcc -o makeInstructions makeInstructions.c
./makeInstructions > instructions.h
gcc -x c -g -std=c99 -O3 -fmax-errors=5 -pedantic -Wall -Wextra -o main -lm main.c && ./main
