int test(void)
{
  #define S 9
  unsigned char stack[] = {
    0,   // 9
    1,   // 8
    255, // 7
    3,   // 6
    4,   // 5
    5,   // 4
    6,   // 3
    7,   // 2
    8,   // 1
    9,   // 0
    255,255,255,255};
  unsigned char inst[4];

  puts("testing instructions");

  for (int i = 0; i < INSTRUCTIONS; ++i)
  {
    unsigned char op;
    char name[4];

#define GO(x,y,z,expect) \
  inst[0] = i; inst[1] = x; inst[2] = y; inst[3] = z; \
  getOpcodeInfo(i,name,&op); \
  printf("%s: ",name); \
  runInstruction(stack,S,inst); \
  if (stack[S + 1] == expect) puts("OK"); else { printf("ERROR (got %d, not %d)",stack[S + 1],expect); return 0; }
  
    switch (i)
    {
      case OPCODE_VAL:
        GO(40,31,20,40)
        break;

      case OPCODE_MOV:
        GO(0,1,2,9)
        GO(1,2,2,8)
        break;

      case OPCODE_MVR:
        GO(9,1,2,9)
        break;
    
      case OPCODE_IFE:
        GO(2,1,4,8)
        GO(9,1,2,7)
        break;

      case OPCODE_INC:
        GO(0,1,2,10)
        GO(7,1,2,0)
        break;

      case OPCODE_DEC:
        GO(9,1,2,255)
        GO(8,1,2,0)
        break;
    
      case OPCODE_BIN:
        GO(0,1,2,10)
        GO(7,1,2,255)
        break;
    
      case OPCODE_BDE:
        GO(9,1,2,0)
        GO(8,1,2,0)
        break;

      case OPCODE_ADD:
        GO(6,5,2,7)
        GO(7,6,2,2)
        break;

      case OPCODE_BAD:
        GO(6,5,2,7)
        GO(7,6,2,255)
        break;

      case OPCODE_AD3:
        GO(0,1,2,24)
        break;

      case OPCODE_BA3:
        GO(0,1,2,24)
        GO(0,1,7,255)
        break;

      case OPCODE_ADX:
        GO(4,1,2,35)
        GO(0,1,2,9)
        break;

      case OPCODE_BAX:
        GO(8,1,2,255)
        break;

      case OPCODE_SUB:
        GO(0,1,2,1)
        GO(1,0,2,255)
        break;

      case OPCODE_BSU:
        GO(0,1,2,1)
        GO(1,0,2,0)
        break;

      case OPCODE_SU3:
        GO(7,8,8,253)
        break;

      case OPCODE_BS3:
        GO(8,6,5,0)
        break;
       
      case OPCODE_MUL:
        GO(6,5,1,12)
        break;
    
      case OPCODE_BMU:
        GO(7,6,1,255)
        break;

      case OPCODE_MU3:
        GO(6,4,5,60)
        break;

      case OPCODE_BM3:
        GO(0,1,2,255)
        break;

      case OPCODE_MUX:
        GO(1,0,0,72)
        break;

      case OPCODE_BMX:
        GO(1,0,0,72)
        GO(8,0,0,255)
        break;

      case OPCODE_DIV:
        GO(2,6,0,2)
        GO(2,9,1,0)
        break;

      case OPCODE_MOD:
        GO(2,6,0,1)
        GO(2,9,1,0)
        break;

      case OPCODE_DIS:
        GO(4,7,0,251)
        break;

      case OPCODE_MIN:
        GO(0,1,3,8)
        break;

      case OPCODE_MI3:
        GO(0,1,2,7)
        break;

      case OPCODE_MIX:
        GO(9,1,2,0)
        break;

      case OPCODE_MAX:
        GO(0,1,20,9)
        break;

      case OPCODE_MA3:
        GO(6,5,4,5)
        break;

      case OPCODE_MXX:
        GO(9,5,4,255)
        break;

      case OPCODE_EQU:
        GO(1,2,4,0)
        break;

      case OPCODE_NEQ:
        GO(1,2,4,1)
        break;

      case OPCODE_EQZ:
        GO(9,2,4,1)
        break;

      case OPCODE_NEZ:
        GO(0,2,4,1)
        break;

      case OPCODE_GRT:
        GO(0,1,4,1)
        break;

      case OPCODE_GRE:
        GO(0,1,4,1)
        break;

      case OPCODE_ANL:
        GO(0,0,4,1)
        break;

      case OPCODE_ORL:
        GO(0,9,4,1)
        break;

      case OPCODE_XOL:
        GO(0,9,4,1)
        GO(0,0,4,0)
        break;

      case OPCODE_NAL:
        GO(0,0,4,0)
        break;

      case OPCODE_NOL:
        GO(0,9,4,0)
        break;

      case OPCODE_NXL:
        GO(0,9,4,0)
        GO(0,0,4,1)
        break;

      case OPCODE_ANB:
        GO(7,6,9,3)
        break;

      case OPCODE_ORB:
        GO(7,6,9,255)
        break;

      case OPCODE_XOB:
        GO(7,8,9,254)
        break;

      case OPCODE_NAB:
        GO(7,7,9,0)
        break;

      case OPCODE_NOB:
        GO(7,1,9,0)
        break;

      case OPCODE_NXB:
        GO(7,8,9,1)
        break;

      case OPCODE_NEB:
        GO(7,8,9,0)
        break;

      case OPCODE_GEB:
        GO(8,8,9,0)
        GO(6,9,9,1)
        break;

      case OPCODE_SEB:
        GO(8,2,9,129)
        GO(8,0,1,3)
        break;

      case OPCODE_CLB:
        GO(7,2,9,127)
        GO(8,0,1,1)
        break;

      case OPCODE_FLB:
        GO(6,8,9,1)
        GO(9,2,1,128)
        break;

      case OPCODE_C1B:
        GO(9,8,9,0)
        GO(8,2,11,1)
        GO(7,2,11,8)
        GO(1,2,11,1)
        break;

      case OPCODE_SHL:
        GO(6,8,1,6)
        GO(6,0,1,6)
        break;

      case OPCODE_SHR:
        GO(6,8,1,1)
        GO(6,0,1,1)
        break;

      case OPCODE_ROL:
        GO(4,1,0,5)
        GO(4,8,0,10)
        GO(1,3,0,2)
        break;

      case OPCODE_ROR:
        GO(8,8,0,128)
        GO(0,1,0,9)
        break;

      case OPCODE_REV:
        GO(7,1,2,255)
        GO(8,1,2,128)
        break;

      default: break;
    }
  }
#undef GO
#undef S

  return 1;
}
