#include <stdio.h>
#include <stdint.h>

#include "parameters.h"
#include "instructions.h"
#include "test.h"

#ifndef SEED
  #define SEED 0
#endif

#ifndef LOOP_SLEEP
  #define LOOP_SLEEP 0
#endif

#ifndef INPUT_BYTES
  #define INPUT_BYTES 16              // bytes of program input
#endif

#ifndef OUTPUT_BYTES
  #define OUTPUT_BYTES 32             // bytes of program output
#endif

#ifndef PROGRAM_MAX_LENGTH
  #define PROGRAM_MAX_LENGTH 128      // maximum number of program instructions
#endif

#ifndef POPULATION_SIZE
  #define POPULATION_SIZE 100         // number of programs in the population
#endif

#ifndef POPULATION_ELITE
  #define POPULATION_ELITE 15         /* how many programs among the population
                                         will be treated as the best */
#endif

#ifndef POPULATION_LOSERS
  #define POPULATION_LOSERS 15        /* how many programs among the population
                                         will be treated as the weakest */
#endif

#ifndef MUTATION
  #define MUTATION 8                  // maximum number of mutations on programs                                         
#endif

// derived constants:

#define STACK_SIZE (INPUT_BYTES + PROGRAM_MAX_LENGTH)
#define MAX_DISSIMILARITY ((PROGRAM_MAX_LENGTH - 1) * 4 * 255)

/*
  A program is an array of 0 to PROGRAM_MAX_LENGTH instructions. Each
  instruction consists of its type (defined by opcode) and 0 to 3 operands. Each
  operand is an 8 bit unsigned integer (byte). Instruction opcode is 8 bit with
  the upper 2 bits giving the number of operands. Program has to end with the
  end instruction (OPCODE_END).

  Program is executed in an environment that works on kind of a stack. This
  stack initially consists of all program start data (INPUT_BYTES bytes).
  Operand values can be dereferenced as an index from the top of the stack
  (0 = top, 1 one below top, ...), in such case the value must not point below
  the bottom of the stack. Some instructions interpret operand values as direct
  values in which case they may be allowed to take values that would be
  forbidden as a dereference.

  Instruction are executed one by one, each one computes a value and pushes it
  on top of the stack. Operands are interpreted in one of two ways:
    - as a direct number (X, Y, Z), or
    - as a dereference to the stack (see above)

  After the program finishes, the topmost OUTPUT_BYTES are taken to be the
  computation output (zero filled from left if necessary). 
*/

typedef uint8_t Program[PROGRAM_MAX_LENGTH * 4];
typedef uint8_t Input[INPUT_BYTES];
typedef uint8_t Output[OUTPUT_BYTES];

uint32_t randomValue = 0;

void randomSeed(uint16_t s)
{
  randomValue = s;
}

uint16_t random(void)
{
  randomValue = (32310901 * (randomValue + 12345));
  return randomValue >> 16;
}

void programInit(Program program)
{
  for (int i = 0; i < PROGRAM_MAX_LENGTH * 4; ++i, ++program)
    *program = 0;
}

void programCopy(const Program programFrom, Program programTo)
{
  for (int i = 0; i < (PROGRAM_MAX_LENGTH * 4); ++i)
  {
    *programTo = *programFrom;
    programTo++;
    programFrom++;
  }
}

void programSanitizeOperands(Program program)
{
  int top = INPUT_BYTES;

  while (1)
  {
    unsigned char opcode = *program;

    if (opcode == OPCODE_END)
      break;

    if (opcode != OPCODE_VAL)
    {
      *(program + 1) = *(program + 1) % top;
      *(program + 2) = *(program + 2) % top;
      *(program + 3) = *(program + 3) % top;
    }

    program += 4;
    top++;
  }
}

void programRandom(Program program)
{
  programInit(program);

  int length = 1 + random() % (PROGRAM_MAX_LENGTH - 2);

  unsigned char *p = program;

  for (int i = 0; i < length; ++i)
  {
    *program = 1 + random() % (INSTRUCTIONS - 1);
    program++;
    *program = random() % 256;
    program++;
    *program = random() % 256;
    program++;
    *program = random() % 256;
    program++;
  }

  programSanitizeOperands(p);
}

int programLength(const Program program)
{
  int result = 0;

  while (*program != OPCODE_END)
  {
    result++;
    program += 4;
  }

  return result;
}

void programMutate(Program program, int amount)
{
  int length = programLength(program);

  unsigned char *p = program;

  for (int i = 0; i < amount; ++i)
  {
    program = p;

    int position = (random() % length) * 4;

    int option = random() % 4;

    if (option == 0 && length <= 1)
      option = 3;
    else if (option == 3 && length >= PROGRAM_MAX_LENGTH - 1)
      option = 0;

    switch (option)
    {
      case 0: // remove instruction
      {
        program = program + position;

        while (1)
        {
          *program = *(program + 4);
          program++;
          *program = *(program + 4);
          program++;
          *program = *(program + 4);
          program++;
          *program = *(program + 4);
          program++;

          if (*program == OPCODE_END)
            break;
        }

        length--;

        break;
      }

      case 1: // modify opcode
        program[position] = 1 + random() % (INSTRUCTIONS - 1);
        break;

      case 2: // modify parameters
        program += position + 1;
        *program = random() % 256;
        program++;
        *program = random() % 256;
        program++;
        *program = random() % 256;

        break;

      case 3: // add instruction
      {
        unsigned char inst[4];

        inst[0] = 1 + random() % (INSTRUCTIONS - 1);
        inst[1] = random() % 256;
        inst[2] = random() % 256;
        inst[3] = random() % 256;

        program += position;

        while (1)
        {
          for (int i = 0; i < 4; ++i)
          {
            unsigned char c = inst[i];
            inst[i] = *(program + i);
            *(program + i) = c;
          }

          if (inst[0] == OPCODE_END)
            break;

          program += 4;
        }
 
        length++;

        break;
      }

      default: break;
    }
  }

  programSanitizeOperands(p);
}

void programCombine(const Program programFrom, Program programTo)
{
  int 
    lShort = programLength(programFrom),
    lLong = programLength(programTo);

  unsigned char *p = programTo;

  const unsigned char
    *pShort = programFrom, 
    *pLong = programTo;

  if (lLong < lShort)
  {
    int tmp = lShort;
    lShort = lLong;
    lLong = tmp;
    pShort = programTo;
    pLong = programFrom;
  }

  for (int i = 0; i < lShort; ++i)
  {
    const unsigned char *src = random() % 2 ? pShort : pLong;

    for (int j = 0; j < 4; ++j)
    {
      *programTo = *src;
      programTo++;
      src++;
    }

    pShort += 4;
    pLong += 4;
  }

  for (int i = 0; i < (lLong - lShort) * 4; ++i)
  {
    *programTo = *pLong;
    pLong++;
    programTo++;
  }

  programSanitizeOperands(p);
}

void programPrintBrief(const Program program)
{
  char name[4];
  unsigned char operands;

  for (int i = 0; i < 8; ++i)
  {
    unsigned char instruction = *program;

    getOpcodeInfo(instruction,name,&operands);

    if (instruction == OPCODE_END)
      return;  
  
    printf("%s ",name);

    program += 4;
  }

  printf("...");
}

void programPrint(const Program program)
{
  char name[4];
  unsigned char operands;

  puts("PROGRAM:");

  while (1)
  {
    unsigned char instruction = *program;

    if (instruction == OPCODE_END)
      break;

    getOpcodeInfo(instruction,name,&operands);

    printf("  %s (%d): ",name,instruction);

    for (int i = 0; i < operands; ++i)
    {
      uint8_t p = *(program + i + 1);
      printf(" %d",p);
    }

    putchar('\n');

    program += 4;
  }
}

void printArray(unsigned char *array, int length)
{
  putchar('{');

  uint8_t first = 1;

  for (int i = 0; i < length; ++i)
  {
    if (first)
      first = 0;
    else
      putchar(',');

    printf("%d",array[i]);
  }

  putchar('}');
}

void programExecute(const Program program, Input input, Output output)
{
  unsigned char stack[STACK_SIZE];

  unsigned char *s = stack;
  unsigned char *in = input;

  for (int i = 0; i < INPUT_BYTES; ++i, ++s, ++in)
    *s = *in;

  for (int i = 0; i < STACK_SIZE - INPUT_BYTES; ++i, ++s)
    *s = 0;

  const unsigned char *p = program;

  int top = INPUT_BYTES - 1;

  while (*p != OPCODE_END)
  {
    runInstruction(stack,top,p);
    top++;
    p += 4;
  }

  unsigned char *o = output + OUTPUT_BYTES - 1;
  s = stack + top;

  int zeros = OUTPUT_BYTES - top - 1;

  while (top >= 0)
  {
    *o = *s;

    if (o == output)
      break;

    s--;
    o--;
    top--;
  }

  for (int i = 0; i < zeros; ++i, --o) 
    *o = 0;
}

#define INSTANCE_NAME_SIZE 7

typedef struct
{
  Program program;
  uint16_t score;
  char rated;
  char name[INSTANCE_NAME_SIZE];

  uint32_t mutations;
  uint32_t combinations;
} Instance;

char randomLetter(void)
{
  char r = 'A' + random() % 26;

  return random() % 2 ? r + 32 : r;
}

void instanceInit(Instance *instance)
{
  instance->program[0] = OPCODE_END;
  instance->score = 0;
  instance->rated = 0;

  for (int i = 0; i < INSTANCE_NAME_SIZE - 1; ++i)
    instance->name[i] = randomLetter();

  instance->name[INSTANCE_NAME_SIZE - 1] = 0; 

  instance->mutations = 0;
  instance->combinations = 0;
}

Instance population[POPULATION_SIZE];
uint32_t generation;

uint16_t programRate(const Program program);

void populationSort(void)
{
  // bubble sort:
 
  for (int j = 0; j < POPULATION_SIZE - 1; ++j)
    for (int i = 0; i < POPULATION_SIZE - 1; ++i)
      if (population[i].score < population[i + 1].score)
      {
        Instance inst = population[i];
        population[i] = population[i + 1];
        population[i + 1] = inst;
      }
}

void populationInit(void)
{
  for (int i = 0; i < POPULATION_SIZE; ++i)
  {
    instanceInit(population + i);
    programRandom(population[i].program);
    population[i].score = programRate(population[i].program);
    population[i].rated = 1;
  }

  generation = 0;

  populationSort();
}

void populationPrint(int limit)
{
  printf("population (gen %d):\n",generation);

  if (limit > POPULATION_SIZE)
    limit = POPULATION_SIZE;

  for (int i = 0; i < limit; ++i)
  {
    const Instance *instance = population + i;

    printf("%d:  %s (score %d, length %d, mutations %u, combinations %u): ",i,instance->name,instance->score,programLength(instance->program),instance->mutations,instance->combinations);
    programPrintBrief(instance->program);
    putchar('\n');
  }
}

void populationRunGeneration(void)
{
  Instance populationOld[POPULATION_SIZE]; 

  for (int i = 0; i < POPULATION_SIZE; ++i)
    populationOld[i] = population[i];

  Instance *inst = population;

  // for elite try slight modifications and only keep them if better

  for (int i = 0; i < POPULATION_ELITE; ++i)
  {
    int mutations = 1 + random() % MUTATION;

    programMutate(inst->program,mutations);
    inst->mutations += mutations;

    inst->score = programRate(inst->program);
    inst->rated = 1;

    if (populationOld[i].score > inst->score)
      *inst = populationOld[i];
    else
      inst->name[random() % (INSTANCE_NAME_SIZE - 1)] = randomLetter();

    inst++;
  }

  // for middle population do either mutations or combinations

  for (int i = 0; i < POPULATION_SIZE - POPULATION_ELITE - POPULATION_LOSERS; ++i)
  {
    if (random() % 2)
    {
      int mutations = 1 + random() % MUTATION;

      programMutate(inst->program,mutations);
      inst->mutations += mutations;

      inst->name[random() % (INSTANCE_NAME_SIZE - 1)] = randomLetter();
      inst->score = programRate(inst->program);
      inst->rated = 1;
    }
    else
    {
      int pos1 = POPULATION_LOSERS + random() % (POPULATION_SIZE - POPULATION_LOSERS);
      int pos2 = POPULATION_LOSERS + random() % (POPULATION_SIZE - POPULATION_LOSERS);

      if (pos2 == pos1)
        pos2 += pos2 == POPULATION_LOSERS ? 1 : -1;

      programCombine(populationOld[pos1].program,populationOld[pos2].program);

      inst->combinations = 
        populationOld[pos1].combinations + populationOld[pos2].combinations + 1;
      inst->mutations =
        populationOld[pos1].mutations + populationOld[pos2].mutations;

      programCopy(populationOld[pos2].program,inst->program);

      for (int j = INSTANCE_NAME_SIZE / 2; j < INSTANCE_NAME_SIZE - 1; ++j)
        inst->name[j] = populationOld[pos2].name[j - INSTANCE_NAME_SIZE / 2];

      inst->score = programRate(inst->program);
      inst->rated = 1;
    }

    inst++;
  }

  // remove losers (replace by new random programs):

  for (int i = 0; i < POPULATION_LOSERS; ++i)
  {
    instanceInit(inst);

    programRandom(inst->program);

    inst->score = programRate(inst->program);
    inst->rated = 1;

    inst++;
  }

  populationSort();

  generation++;
}

/*
  Helper function that checks the absolute difference between result and desired
  result and returns up to max points (max point being awarded for exact hit).
*/
int scoreResult(int result, int desired, int max)
{
  result -= desired;
  
  if (result < 0)
    result *= -1;

  return (result < max) ? max - result : 0;
}

#include "simulation.h"
#include "optimize.h"

void programPrintC(Program p)
{
  printProgramC(p,INPUT_BYTES,OUTPUT_BYTES,PROGRAM_MAX_LENGTH);
}

int main(void)
{
/*
  test();
  testOptimization();
  return 0;
*/
  randomSeed(SEED);

  populationInit();

  while (1)
  {
    if (generation % 1000 == 0)
    {
      puts("\n\n\n\n\n\n");
      populationPrint(20);
 
      putchar('\n');
      programPrint(population[0].program);
      printArray(population[0].program,PROGRAM_MAX_LENGTH * 4);
      putchar('\n');
    }

#if 0
    // experimental: allowing optimization is questionable (can weaken mutations)

    if (generation % 16 == 0)
    {
      for (int i = 0; i < POPULATION_ELITE; ++i)
        programOptimize(population[i].program);
    }
#endif

    populationRunGeneration();
    usleep(LOOP_SLEEP);
  }

  return 0;
}
