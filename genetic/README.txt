Quick and not so nice C "framework" for genetic programming. Includes a small
custom instruction set/language working on a stack (description in main.c).

As a user of this modify parameters.h and simulation.h.

Now this is set up to learn a simple operation but I wanted this to be usable
for learning more complex functions such as board evaluation in chess.

Released under CC0 1.0, public domain.
