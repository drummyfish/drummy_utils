/*
  Here you should define the key functions that will drive the simulation. Don't
  forget to also modify parameters.h.
*/

// this evolves the sin function

#include <math.h>

uint8_t sin8(uint8_t x)
{
  return 128 + sin((x * 3.141519 * 2.0) / 256.0) * 128;
}

uint16_t programRate(const Program program)
{
  Input in;
  Output out;

  uint16_t r = PROGRAM_MAX_LENGTH - programLength(program); // penalize length a bit

  for (int i = 0; i < 256; ++i)
  {

    uint16_t a = i * 13;
    uint16_t res = sin8(i);

    in[0] = a;
    
    programExecute(program,in,out);

    r += scoreResult(out[0],res,3) * 8; // * 8 for greater weight than length
  }

  return r;
}
