#include <stdio.h>

/*
  x    (unsigned char): 1st instruction operand
  y    (unsigned char): 2nd instruction operand
  z    (unsigned char): 3rd instruction operand
  t    (uint16_t):      current stack size
  s(N) (unsigned char): Nth stack value from the top
  r    (unsigned char): result value
  v32  (int32_t):       temporary 32 bit integer
*/

const char *instructionDefs[] =
{
  "END", "0", "end program",                              "r = 0;",
  "VAL", "1", "load constant value",                      "r = x;",
  "MOV", "1", "load value",                               "r = s(x);",
  "MVR", "1", "load referenced value",                    "r = s(s(x) % t);",
  "IFE", "3", "load value based on condition",            "r = s(x) ? s(y) : s(z);",

  "INC", "1", "increment value",                          "r = s(x) + 1;",        
  "DEC", "1", "decrement value",                          "r = s(x) - 1;",        
  "BIN", "1", "increment value (bounded)",                "r = s(x) + ((s(x) < 255) ? 1 : 0);",        
  "BDE", "1", "decrement value (bounded)",                "r = s(x) - ((s(x) > 0) ? 1 : 0);",        

  "ADD", "2", "add 2 values",                             "r = s(x) + s(y);",        
  "BAD", "2", "add 2 values (bounded)",                   "v32 = s(x) + s(y); r = v32 < 256 ? v32 : 255;", 
  "AD3", "3", "add 3 values",                             "r = s(x) + s(y) + s(z);", 
  "BA3", "3", "add 3 values (bounded)",                   "v32 = s(x) + s(y) + s(z); r = v32 < 256 ? v32 : 255;", 
  "ADX", "1", "add x + 1 previous values",                "for (unsigned char i = 0; i < x + 1; ++i) { r += s(i); }",
  "BAX", "1", "add x + 1 previous values (bounded)",      "for (unsigned char i = 0; i < x + 1; ++i) { v32 += s(i); if (v32 > 255) {v32 = 255; break;} } r = v32;",

  "SUB", "2", "substract 2 values",                       "r = s(x) - s(y);",        
  "BSU", "2", "substract 2 values (bounded)",             "v32 = s(x) - s(y); r = v32 >= 0 ? v32 : 0;", 
  "SU3", "3", "substract 3 values",                       "r = s(x) - s(y) - s(z);", 
  "BS3", "3", "substract 3 values (bounded)",             "v32 = s(x) - s(y) - s(z); r = v32 >= 0 ? v32 : 0;",

  "MUL", "2", "multiply 2 values",                        "r = s(x) * s(y);",        
  "BMU", "2", "multiply 2 values (bounded)",              "v32 = s(x) * s(y); r = v32 < 256 ? v32 : 255;",
  "MU3", "3", "multiply 3 values",                        "r = s(x) * s(y) * s(z);", 
  "BM3", "3", "multiply 3 values (bounded)",              "v32 = s(x) * s(y) * s(z); r = v32 < 256 ? v32 : 255;", 
  "MUX", "1", "multiply x + 1 previous values",           "r = 1; for (unsigned char i = 0; i < x + 1; ++i) { r *= s(i); }",
  "BMX", "1", "multiply x + 1 previous values (bounded)", "v32 = 1; for (unsigned char i = 0; i < x + 1; ++i) { v32 *= s(i); if (v32 > 255) {v32 = 255; break;} } r = v32;",

  "DIV", "2", "divide 2 values",                          "r = s(y) != 0 ? s(x) / s(y) : 0;",
  "MOD", "2", "compute division remainder",               "r = s(y) != 0 ? s(x) % s(y) : 0;",

  "DIS", "2", "divide 2 signed values",                   "r = s(y) != 0 ? (unsigned char) (((signed char) s(x)) / ((signed char) s(y))) : 0;",

  "MIN", "2", "get minimum of 2 values",                  "r = s(x) < s(y) ? s(x) : s(y);",
  "MI3", "3", "get minimum of 3 values",                  "r = s(x) < s(y) ? (s(x) < s(z) ? s(x) : s(z)) : (s(y) < s(z) ? s(y) : s(z));",
  "MIX", "3", "get minimum of x + 1 previous values",     "r = 255; for (unsigned char i = 0; i < x + 1; ++i) if (s(i) < r) r = s(i);",

  "MAX", "2", "get maximum of 2 values",                  "r = s(x) > s(y) ? s(x) : s(y);",
  "MA3", "3", "get maximum of 3 values",                  "r = s(x) > s(y) ? (s(x) > s(z) ? s(x) : s(z)) : (s(y) > s(z) ? s(y) : s(z));",
  "MXX", "3", "get maximum of x + 1 previous values",     "r = 0; for (unsigned char i = 0; i < x + 1; ++i) if (s(i) > r) r = s(i);",

  "EQU", "2", "test equality",                            "r = s(x) == s(y);",
  "NEQ", "2", "test nonequality",                         "r = s(x) != s(y);",
  "EQZ", "1", "test equality to 0",                       "r = s(x) == 0;",
  "NEZ", "1", "test inequality to 0",                     "r = s(x) != 0;",
  "GRT", "2", "test greater than inequality",             "r = s(x) > s(y);",
  "GRE", "2", "test greater than or equal relation",      "r = s(x) >= s(y);",

  "ANL", "2", "perform logical and",                      "r = s(x) && s(y);",
  "ORL", "2", "perform logical or",                       "r = s(x) || s(y);",
  "XOL", "2", "perform logical xor",                      "r = (s(x) == 0) != (s(y) == 0);",
  "NAL", "2", "perform logical nand",                     "r = !(s(x) && s(y));",
  "NOL", "2", "perform logical nor",                      "r = !(s(x) || s(y));",
  "NXL", "2", "perform logical nxor",                     "r = (s(x) == 0) == (s(y) == 0);",
  
  "ANB", "2", "perform bitwise and",                      "r = s(x) & s(y);",
  "ORB", "2", "perform bitwise or",                       "r = s(x) | s(y);",
  "XOB", "2", "perform bitwise xor",                      "r = s(x) ^ s(y);",
  "NAB", "2", "perform bitwise nand",                     "r = ~(s(x) & s(y));",
  "NOB", "2", "perform bitwise nor",                      "r = ~(s(x) | s(y));",
  "NXB", "2", "perform bitwise nxor",                     "r = ~(s(x) ^ s(y));",
  "NEB", "1", "perform bitwise negation",                 "r = ~s(x);",

  "GEB", "2", "get bit",                                  "r = (s(x) >> (s(y) % 8)) & 0x01;", 
  "SEB", "2", "set bit",                                  "r = s(x) | (0x01 << (s(y) % 8));", 
  "CLB", "2", "clear bit",                                "r = s(x) & ~(0x01 << (s(y) % 8));", 
  "FLB", "2", "flip bit",                                 "r = s(x) ^ (0x01 << s(y));", 

  "C1B", "2", "count 1 bits",                             "r = 0; for (unsigned char i = 0; i < 8; ++i) if ((s(x) >> i) & 0x01) r++;", 

  "SHL", "2", "shift left",                               "r = s(x) << (s(y) % 8);",
  "SHR", "2", "shift right",                              "r = s(x) >> (s(y) % 8);",
  "ROL", "2", "rotate left",                              "v8 = s(y) % 8; r = v8 != 0 ? (s(x) << v8) | (s(x) >> (8 - v8)) : s(x);",
  "ROR", "2", "rotate right",                             "v8 = s(y) % 8; r = v8 != 0 ? (s(x) >> v8) | (s(x) << (8 - v8)) : s(x);",
  "REV", "2", "reverse bits",                             "v8 = s(x); r = v8 & 0x01; for (unsigned char i = 0; i < 7; ++i) {r <<= 1; v8 >>= 1; r |= v8 & 0x01;}"
};

int main(void)
{
  int instructionCount = sizeof(instructionDefs) / (sizeof(char *) * 4);

  puts("// generated by makeInstructions.c, do NOT edit manually\n");
  printf("#define INSTRUCTIONS %d // total number of instructions\n\n",instructionCount);

  for (int i = 0; i < instructionCount; ++i)
  {
    const char **s = instructionDefs + i * 4;
    printf("#define OPCODE_%s 0x%02x // (%s) %s\n",s[0],i,s[1],s[2]);
  }

  puts(
    "\nvoid getOpcodeInfo(unsigned char opcode, char name[4], unsigned char *operands)\n"
    "{\n  name[0] = '?'; name[1] = '?'; name[2] = '?'; name[3] = 0;\n\n  switch (opcode)\n  {");

  for (int i = 0; i < instructionCount; ++i)
  {
    const char **s = instructionDefs + i * 4;
    printf("    case OPCODE_%s: *operands = %s; name[0] = '%c'; name[1] = '%c'; name[2] = '%c'; break;\n",
    s[0],s[1],s[0][0],s[0][1],s[0][2]);
  }

  puts("    default: *operands = 0; break;\n  }\n}\n");

  puts("void runInstruction(unsigned char *stack, int top, const unsigned char *instruction)\n{\n"
    "  unsigned char r = 0, v8 = 0, x = *(instruction + 1), y = *(instruction + 2), z = *(instruction + 3);\n"
    "  int t = top + 1;\n"
    "  int32_t v32 = 0;\n\n"
    "  #define s(n) stack[top - (n)]\n\n"
    "  switch (*instruction)\n  {");

  for (int i = 0; i < instructionCount; ++i)
  {
    const char **s = instructionDefs + i * 4;
    printf("    case OPCODE_%s: %s break;\n",s[0],s[3]);
  }

  puts("    default: break;\n  }\n\n  stack[t] = r;\n\n  #undef s\n}");

/*

  void algorithm(unsigned char input[INPUT_BYTES], unsigned char output[OUTPUT_BYTES])
  {
    unsigned char stack[STACK_SIZE];

    for (int i = 0; i < STACK_SIZE; ++i)
      stack[i] = i < INPUT_BYTES ? input[i] : 0;

    unsigned char *stackTop = stack + INPUT_BYTES - 1;

    #define s(n) (*(stack - (n)))

    unsigned char x = 0, y = 0, z = 0;
    uint16_t t = INPUT_BYTES;
    
    x = ...; y = ...; z = ...;
    ...
    t++;

    ...

    unsigned char *o = output + OUTPUT_BYTES - 1;
    int st = stack + t;

    int zeros = OUTPUT_BYTES - t - 1;

    while (t >= 0)
    {
      *o = *s;

      if (o == output)
        break;

      st--;
      o--;
      t--;
    }

    for (int i = 0; i < zeros; ++i, --o) 
      *o = 0;
  }
*/

  puts(
    "\nvoid printProgramC(unsigned char *program, int inputBytes, int outputBytes, int programMaxLength)\n{\n"
    "  printf(\"void algorithm(unsigned char input[%d], unsigned char output[%d])\\n{\\n  \",inputBytes,outputBytes);\n"
    "  printf(\"unsigned char stack[%d];\\n\\n\",inputBytes + programMaxLength);\n"
    "  printf(\"  for (int i = 0; i < %d; ++i)\\n\",inputBytes + programMaxLength);\n"
    "  printf(\"    stack[i] = i < %d ? input[i] : 0;\\n\\n\",inputBytes);\n"
    "  printf(\"  unsigned char *stackTop = stack + %d - 1;\\n\\n\",inputBytes);\n"
    "  puts  (\"  #define s(n) (*(stackTop - (n)))\");\n"
    "  puts  (\"  unsigned char x = 0, y = 0, z = 0, r = 0;\");\n"
    "  puts  (\"  long int v32 = 0;\\n  unsigned char v8 = 0;\");\n"
    "  printf(\"  int t = %d;\\n\\n\",inputBytes - 1);\n\n"

    "  while (*program != OPCODE_END)\n  {  \n"
    "    printf(\"  x = %d; y = %d; z = %d; r = 0;\\n\",*(program + 1),*(program + 2),*(program + 3));\n"
    "    const char *instr = \"\";\n\n"
    "    switch (*program)\n    {\n");

  for (int i = 0; i < instructionCount; ++i)
  {
    const char **s = instructionDefs + i * 4;
    printf("    case OPCODE_%s: puts(\"  %s // %s\"); break;\n",s[0],s[3],s[0]);
  }

  puts(
    "    default: break;\n    }\n\n"
    "    puts  (\"  stackTop++; *stackTop = r; t++;\\n\");\n"
    "    program += 4;\n  }\n\n"

    "  printf(\"  unsigned char *o = output + %d - 1;\\n\",outputBytes);\n"
    "  puts  (\"  unsigned char *st = stack + t;\\n\");\n"
    "  printf(\"  int zeros = %d - t - 1;\\n\\n\",outputBytes);\n"
    "  puts  (\"  while (t >= 0)\\n  {\");\n"
    "  puts  (\"    *o = *st;\\n\");\n"
    "  puts  (\"    if (o == output)\\n      break;\\n\");\n"
    "  puts  (\"    st--; o--; t--;\\n  }\\n\");\n"
    "  puts  (\"  for (int i = 0; i < zeros; ++i, --o)\\n    *o = 0;\\n}\");\n}");

  return 0;
}
