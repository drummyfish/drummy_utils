#!/bin/bash

# Adds mirror remotes to a git repot, expects one parameter: the repo name.

echo $1

git remote | xargs -n1 git remote remove # delete all remotes

git remote add origin https://gitlab.com/drummyfish/$1
git remote set-url --add --push origin https://gitlab.com/drummyfish/$1
git remote set-url --add --push origin git@git.sr.ht:~drummyfish/$1
git remote set-url --add --push origin https://codeberg.org/drummyfish/$1.git

git remote -v

git push --set-upstream origin master
