/*  Copyright 2010-2011 Guillaume Duhamel

    This file is part of Gophrier.

    Gophrier is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    Gophrier is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Gophrier; if not, write to the Free Software
    Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301  USA
*/

#include "dbstring.h"

#include <stdlib.h>
#include <stdio.h>

static void dbstring_swap(DBString * string);

DBString * dbstring_new(int buflen)
{
    DBString * string;

    string = malloc(sizeof(DBString));
    string->front = 0;
    string->back = 1;
    string->buffer[0] = malloc(1 + buflen);
    string->buffer[1] = malloc(1 + buflen);
    string->buffer[0][0] = string->buffer[1][0] = 0;

    return string;
}

void dbstring_free(DBString * string)
{
    free(string->buffer[0]);
    free(string->buffer[1]);
    free(string);
}

void dbstring_prepend(DBString * string, const char * toprepend)
{
    sprintf(string->buffer[string->back], "%s%s", toprepend, string->buffer[string->front]);
    dbstring_swap(string);
}

void dbstring_shift(DBString * string, int toshift)
{
    sprintf(string->buffer[string->back], "%s", string->buffer[string->front] + toshift);
    dbstring_swap(string);
}

char * dbstring_content(DBString * string)
{
    return string->buffer[string->front];
}

static void dbstring_swap(DBString * string)
{
    string->front = 1 - string->front;
    string->back = 1 - string->back;
}
