/*  Copyright 2010-2011 Guillaume Duhamel

    This file is part of Gophrier.

    Gophrier is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    Gophrier is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Gophrier; if not, write to the Free Software
    Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301  USA
*/

#include "map.h"
#include "sock.h"
#include "request.h"
#include "path.h"
#include "conf.h"

#include <stdlib.h>
#include <string.h>
#include <fcntl.h>
#include <unistd.h>

void map_parse(Request * request)
{
    FILE * map;
    char buffer[1024];
    char type;
    char name[1024], path[1024], host[1024];
    int port, matched;

    map = fopen(request->fullpath->path, "r");
    while(fgets(buffer, 1024, map))
    {
        matched = sscanf(buffer, "%c%[^\t\n]%*1[\t]%[^\t\n]%*1[\t]%[^\t\n]%*1[\t]%d", &type, name, path, host, &port);
        if (matched > 2)
        {
            Path * absname;
            absname = path_new(path, request->path->path);
            strcpy(path, absname->path);
            path_free(absname);
        }
        if (matched == 5)
            request_send(request, buffer, strlen(buffer));
        else if (matched == 3)
        {
            if (type == 'i')
                request_push(request, type, name, path, "(NULL)", 0);
            else
                request_push(request, type, name, path, request->sockname, conf_get_port());
        }
        else
        {
            char * eol;
            eol = strchr(buffer, '\n');
            if (eol)
                *eol = '\0';
            request_push(request, 'i', buffer, "fake", "(NULL)", 0);
        }
    }
    fclose(map);

    request_close(request);
}

Path * map_path(const Path * inpath)
{
    Path * path;
    List * names;
    char * name;

    if (! conf_get_gm_enable()) return NULL;

    names = conf_get_gm_names();
    while(names)
    {
        name = names->current;

        path = path_new(name, inpath->path);
        if (path->valid && (! path_is_dir(path)))
            return path;
        else
        {
            path_free(path);
            path = NULL;
        }

        names = names->next;
    }

    return NULL;
}
