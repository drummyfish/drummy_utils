/*  Copyright 2011 Guillaume Duhamel

    This file is part of Gophrier.

    Gophrier is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    Gophrier is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Gophrier; if not, write to the Free Software
    Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301  USA
*/

#include "../hand.h"
#include "../sock.h"
#include "../pool.h"

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <signal.h>
#include <sys/signalfd.h>

Socket * hand_new()
{
    Socket * sock;
    sigset_t mask;
    int fd;

    sigemptyset(&mask);
    sigaddset(&mask, SIGINT);
    sigaddset(&mask, SIGQUIT);
    sigprocmask(SIG_BLOCK, &mask, NULL);
    fd = signalfd(-1, &mask, 0);

    sock = (Socket *) malloc(sizeof(Socket));
    sock->fd = fd;
    sock->onRead = hand_on_read;

    return sock;
}

void hand_on_read(Socket * sock, SocketPool * pool)
{
    struct signalfd_siginfo fdsi;

    read(sock->fd, &fdsi, sizeof(struct signalfd_siginfo));
    if (fdsi.ssi_signo == SIGINT)
        socket_pool_reload(pool);
    if (fdsi.ssi_signo == SIGQUIT)
        socket_pool_stop(pool);
}
