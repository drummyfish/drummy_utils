/*  Copyright 2010-2011 Guillaume Duhamel

    This file is part of Gophrier.

    Gophrier is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    Gophrier is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Gophrier; if not, write to the Free Software
    Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301  USA
*/

#include "conf.h"
#include "path.h"
#include "log.h"

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

static Path * confdir = NULL;
static Path * rootdir = NULL;
static Path * logdir = NULL;
static Path * moddir = NULL;
static Path * pidpath = NULL;
static int isdaemon = -1;
static int useipv6 = -1;
static int port = -1;
static int browsable = -1;
static char * typehelper = NULL;
static int symlinks = -1;
static int hidden = -1;
static int gm_enable = -1;
static List * gm_names = NULL;
static char * user = NULL;
static char * group = NULL;
static char * host = NULL;

static FILE * conf_open(const char * conf);
static int conf_read_int(const char * confkey, int defaultvalue);
static char * conf_read_string(const char * confkey, const char * defaultvalue);
static Path * conf_read_path(const char * confkey, const char * defaultvalue);
static List * conf_read_string_list(const char * confkey, const char * defaultvalue);

void conf_reset()
{
    Path ** pathconfs[] = { &rootdir, &logdir, &moddir };
    char ** stringconfs[] = { &typehelper };
    int i, sclen;

    sclen = sizeof(pathconfs) / sizeof(pathconfs[0]);

    for(i = 0;i < sclen;i++)
        if (*pathconfs[i])
        {
            path_free(*pathconfs[i]);
            *pathconfs[i] = NULL;
        }

    sclen = sizeof(stringconfs) / sizeof(stringconfs[0]);

    for(i = 0;i < sclen;i++)
        if (*stringconfs[i])
        {
            free(*stringconfs[i]);
            *stringconfs[i] = NULL;
        }

    browsable = -1;
    symlinks = -1;
    hidden = -1;

    gm_enable = -1;
    list_free(gm_names);
    gm_names = NULL;

    log_reopen();
}

char * conf_get_conf_dir()
{
    if (! confdir)
        confdir = path_new(GOPHRIER_CONF_DIR, NULL);

    return confdir->path;
}

int conf_set_conf_dir(const char * path)
{
    if (confdir)
        path_free(confdir);

    confdir = path_new(path, NULL);
    return 0;
}

char * conf_get_root_dir()
{
    if (! rootdir)
        rootdir = conf_read_path("rootdir", GOPHRIER_DEFAULT_ROOT_DIR);

    return rootdir->path;
}

int conf_set_root_dir(const char * path)
{
    if (rootdir)
        path_free(rootdir);

    rootdir = path_new(path, NULL);
    return 0;
}

char * conf_get_log_dir()
{
    if (! logdir)
        logdir = conf_read_path("logdir", GOPHRIER_DEFAULT_LOG_DIR);

    return logdir->path;
}

int conf_set_log_dir(const char * path)
{
    if (logdir)
        path_free(logdir);

    logdir = path_new(path, NULL);
    log_reopen();
    return 0;
}

#ifdef GOPHRIER_MOD_ENABLED
char * conf_get_mod_dir()
{
    if (! moddir)
        moddir = conf_read_path("moddir", GOPHRIER_MOD_DIR);

    return moddir->path;
}

int conf_set_mod_dir(const char * path)
{
    if (moddir)
        path_free(moddir);

    moddir = path_new(path, NULL);
    return 0;
}
#endif

char * conf_get_pid_path()
{
    if (! pidpath)
        pidpath = conf_read_path("pidpath", GOPHRIER_PID_PATH);

    return pidpath->path;
}

int conf_set_pid_path(const char * path)
{
    if (pidpath)
        path_free(pidpath);

    pidpath = path_new(path, NULL);
    return 0;
}

int conf_is_daemon()
{
    if (isdaemon == -1)
        isdaemon = conf_read_int("daemon", GOPHRIER_DEFAULT_DAEMON);

    return isdaemon;
}

int conf_set_daemon(int d)
{
    /* should check if gophrier is running */
    isdaemon = d;

    return 0;
}

int conf_use_ipv6()
{
    if (useipv6 == -1)
        useipv6 = conf_read_int("ipv6", GOPHRIER_DEFAULT_IPV6);

    return useipv6;
}

int conf_set_ipv6(int i)
{
    useipv6 = i;

    return 0;
}

int conf_get_port()
{
    if (port == -1)
        port = conf_read_int("port", GOPHRIER_DEFAULT_PORT);

    return port;
}

int conf_set_port(int p)
{
    /* should check if gophrier is running */
    port = p;

    return 0;
}

int conf_get_browsable()
{
    if (browsable == -1)
        browsable = conf_read_int("browsable", GOPHRIER_DEFAULT_BROWSABLE);

    return browsable;
}

int conf_set_browsable(int enable)
{
    browsable = enable;

    return 0;
}

char * conf_get_type_helper()
{
    if (! typehelper)
        typehelper = conf_read_string("typehelper", GOPHRIER_DEFAULT_TYPE_HELPER);

    return typehelper;
}

int conf_get_symlinks()
{
    if (symlinks == -1)
        symlinks = conf_read_int("symlinks", GOPHRIER_DEFAULT_SYMLINKS);

    return symlinks;
}

int conf_set_symlinks(int enable)
{
    symlinks = enable;

    return 0;
}

int conf_get_hidden()
{
    if (hidden == -1)
        hidden = conf_read_int("hidden", GOPHRIER_DEFAULT_SHOW_HIDDEN);

    return hidden;
}

int conf_set_hidden(int enable)
{
    hidden = enable;

    return 0;
}

char * conf_get_user()
{
    if (user == NULL)
        user = conf_read_string("user", "");

    return user;
}

int conf_set_user(const char * u)
{
    if (user)
        free(user);
    user = strdup(u);

    return 0;
}

char * conf_get_group()
{
    if (group == NULL)
        group = conf_read_string("group", "");

    return group;
}

int conf_set_group(const char * g)
{
    if (group)
        free(group);
    group = strdup(g);

    return 0;
}

char * conf_get_host()
{
    if (host == NULL)
        host = conf_read_string("host", "");

    return host;
}

int conf_set_host(const char * h)
{
    if (host)
        free(host);
    host = strdup(h);

    return 0;
}

int conf_get_gm_enable()
{
    if (gm_enable == -1)
        gm_enable = conf_read_int("gophermap/enable", GOPHRIER_DEFAULT_GM_ENABLE);

    return gm_enable;
}

int conf_set_gm_enable(int enable)
{
    gm_enable = enable;

    return 0;
}

List * conf_get_gm_names()
{
    if (! gm_names)
        gm_names = conf_read_string_list("gophermap/names", GOPHRIER_DEFAULT_GM_NAME);

    return gm_names;
}

static int conf_read_int(const char * confkey, int defaultvalue)
{
    char buffer[1024];
    FILE * conffile;
    int value;
    
    conffile = conf_open(confkey);
    if (! conffile) return defaultvalue;

    if (NULL == fgets(buffer, 1024, conffile)) return defaultvalue;
    value = strtol(buffer, NULL, 10);
    fclose(conffile);
    return value;
}

static char * conf_read_string(const char * confkey, const char * defaultvalue)
{
    char buffer[1024];
    FILE * conffile;
    char * value;
    
    conffile = conf_open(confkey);
    if (! conffile) return strdup(defaultvalue);
    
    if (NULL == fgets(buffer, 1024, conffile)) return strdup(defaultvalue);
    {
        char * fixnl;
        fixnl = strchr(buffer, '\n');
        if (fixnl)
            *fixnl = 0;
    }
    value = strdup(buffer);
    fclose(conffile);
    return value;
}

static Path * conf_read_path(const char * confkey, const char * defaultvalue)
{
    char buffer[1024];
    FILE * conffile;
    Path * value;

    conffile = conf_open(confkey);
    if (! conffile) return path_new(defaultvalue, NULL);

    if (NULL == fgets(buffer, 1024, conffile)) return path_new(defaultvalue, NULL);
    {
        char * fixnl;
        fixnl = strchr(buffer, '\n');
        if (fixnl)
            *fixnl = 0;
    }
    value = path_new(buffer, NULL);
    fclose(conffile);
    return value;
}

static List * conf_read_string_list(const char * confkey, const char * defaultvalue)
{
    char buffer[1024];
    FILE * conffile;
    List * list = NULL;

    conffile = conf_open(confkey);
    if (! conffile)
    {
        list = list_push(list, strdup(defaultvalue));
        return list;
    }

    while(fgets(buffer, 1024, conffile))
    {
        char * fixnl;
        fixnl = strchr(buffer, '\n');
        if (fixnl)
            *fixnl = 0;
        list = list_push(list, strdup(buffer));
    }

    if (NULL == list)
    {
        list = list_push(list, strdup(defaultvalue));
    }

    fclose(conffile);
    return list;
}

static FILE * conf_open(const char * conf)
{
    char * fullconf;
    char * confdir;
    FILE * file;
    
    confdir = conf_get_conf_dir();
    
    fullconf = (char *) malloc(strlen(confdir) + 1 + strlen(conf) + 1);
    sprintf(fullconf, "%s/%s", confdir, conf);
    file = fopen(fullconf, "r");
    free(fullconf);

    return file;
}
