#ifndef GOPHRIER_MOD_H
#define GOPHRIER_MOD_H

typedef enum { MOD_TYPE_HELPER } mod_type;

void * mod_load(const char * name, mod_type type);

#endif
