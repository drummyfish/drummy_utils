/*  Copyright 2011 Guillaume Duhamel

    This file is part of Gophrier.

    Gophrier is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    Gophrier is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Gophrier; if not, write to the Free Software
    Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301  USA
*/

#include "hand.h"
#include "pool.h"

#include <unistd.h>
#include <signal.h>

static int hand_in;

void hand_on_signal(int signum)
{
    write(hand_in, &signum, sizeof(int));
}

Socket * hand_new()
{
    Socket * sock;
    struct sigaction act;
    int fd[2];

    pipe(fd);
    hand_in = fd[1];
    sock = socket_new(fd[0]);
    sock->onRead = hand_on_read;

    memset(&act, 0, sizeof(struct sigaction));
    act.sa_handler = hand_on_signal;

    sigaction(SIGINT, &act, NULL);
    sigaction(SIGQUIT, &act, NULL);

    return sock;
}

void hand_on_read(Socket * sock, SocketPool * pool)
{
    int signum;
    read(sock->fd, &signum, sizeof(int));
    if (signum == SIGINT)
        socket_pool_reload(pool);
    if (signum == SIGQUIT)
        socket_pool_stop(pool);
}
