/*  Copyright 2010-2011 Guillaume Duhamel

    This file is part of Gophrier.

    Gophrier is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    Gophrier is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Gophrier; if not, write to the Free Software
    Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301  USA
*/

#include <sys/socket.h>
#include <stdio.h>
#include <errno.h>
#include <limits.h>

#include "client.h"
#include "pool.h"
#include "prerequest.h"
#include "request.h"
#include "log.h"

static void client_socket_read(Socket * sock);

Socket * client_socket_new(int fd)
{
    Socket * sock;

    sock = socket_new(fd);
    sock->extra = pre_request_new();
    sock->onRead = client_socket_on_read;

    return sock;
}

void client_socket_on_read(Socket * sock, SocketPool * pool)
{
    PreRequest * pr = (PreRequest *) sock->extra;

    client_socket_read(sock);

    if (! pr->ok)
        return;

    if (pr->path != NULL)
    {
        Request * request;

        log_append("%s", pr->path);
        
        request = request_new(sock, pr->path);
        pre_request_free(pr);
        request_exec(request);
        request_free(request);
    }

    socket_pool_remove(pool, sock);
}

static void client_socket_read(Socket * sock)
{
    char buffer[1024];
    ssize_t got;
    int j;
    PreRequest * pr = (PreRequest *) sock->extra;

    while(! pr->ok)
    {
        got = recv(sock->fd, buffer, 1024, MSG_DONTWAIT);
        switch(got)
        {
            case -1:
                return;
            case 0:
                pre_request_set_valid(pr, 1);
                break;
            default:
                pr->pathsize += got;
                if (pr->pathsize > PATH_MAX)
                {
                    pre_request_set_valid(pr, 0);
                    break;
                }
                pr->path = realloc(pr->path, pr->pathsize);
                j = 0;
                while((j < got) && (buffer[j] != '\r') && (buffer[j] != '\n'))
                {
                    pr->path[pr->pos] = buffer[j];
                    pr->pos++; j++;
                }
                
                if ((buffer[j] == '\r') || (buffer[j] == '\n'))
                    pre_request_set_valid(pr, 1);
                break;
        }
    }
}
