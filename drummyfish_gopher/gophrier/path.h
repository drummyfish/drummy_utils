/*  Copyright 2010-2011 Guillaume Duhamel

    This file is part of Gophrier.

    Gophrier is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    Gophrier is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Gophrier; if not, write to the Free Software
    Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301  USA
*/

#ifndef GOPHRIER_PATH_H
#define GOPHRIER_PATH_H

#include "list.h"
#include <sys/stat.h>

struct _Path
{
    List * components;
    char * path;
    int sync;
    unsigned int len;
    mode_t mode;
    int valid;
};


/** @brief Sanitize a path to its canonical value.
 *
 * @param inpath The path to sanitize.
 * @param context An optional path used to turn inpath into
 * an absolute path. This value is prepended to inpath before the
 * cleanup is done. It can also be NULL, in this case the current
 * working directory is used as context if inpath is a relative
 * path and no context is used otherwise.
 *
 * Removes spurious characters from a path such as extra dots
 * or slashes and makes it an absolute path according to
 * the context.
 *
 * The returned value have a leading slash and no trailing one.
 *
 * The caller should call path_free on the returned struct.
 */
Path * path_new(const char * inpath, const char * context);
void path_push(Path * path, const char * component);
void path_free(Path * path);
int path_is_link(Path * path);
int path_is_dir(Path * path);
int path_is_executable(Path * path);
int path_is_regular(Path * path);

#endif
