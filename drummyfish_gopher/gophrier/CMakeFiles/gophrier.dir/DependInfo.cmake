# The set of languages for which implicit dependencies are needed:
set(CMAKE_DEPENDS_LANGUAGES
  "C"
  )
# The set of files for implicit dependencies of each language:
set(CMAKE_DEPENDS_CHECK_C
  "/home/tastyfish/Downloads/gophrier/gophrier-0.2.3/cli.c" "/home/tastyfish/Downloads/gophrier/gophrier-0.2.3/CMakeFiles/gophrier.dir/cli.c.o"
  "/home/tastyfish/Downloads/gophrier/gophrier-0.2.3/client.c" "/home/tastyfish/Downloads/gophrier/gophrier-0.2.3/CMakeFiles/gophrier.dir/client.c.o"
  "/home/tastyfish/Downloads/gophrier/gophrier-0.2.3/conf.c" "/home/tastyfish/Downloads/gophrier/gophrier-0.2.3/CMakeFiles/gophrier.dir/conf.c.o"
  "/home/tastyfish/Downloads/gophrier/gophrier-0.2.3/dbstring.c" "/home/tastyfish/Downloads/gophrier/gophrier-0.2.3/CMakeFiles/gophrier.dir/dbstring.c.o"
  "/home/tastyfish/Downloads/gophrier/gophrier-0.2.3/dir.c" "/home/tastyfish/Downloads/gophrier/gophrier-0.2.3/CMakeFiles/gophrier.dir/dir.c.o"
  "/home/tastyfish/Downloads/gophrier/gophrier-0.2.3/error.c" "/home/tastyfish/Downloads/gophrier/gophrier-0.2.3/CMakeFiles/gophrier.dir/error.c.o"
  "/home/tastyfish/Downloads/gophrier/gophrier-0.2.3/exe.c" "/home/tastyfish/Downloads/gophrier/gophrier-0.2.3/CMakeFiles/gophrier.dir/exe.c.o"
  "/home/tastyfish/Downloads/gophrier/gophrier-0.2.3/file.c" "/home/tastyfish/Downloads/gophrier/gophrier-0.2.3/CMakeFiles/gophrier.dir/file.c.o"
  "/home/tastyfish/Downloads/gophrier/gophrier-0.2.3/linux/hand.c" "/home/tastyfish/Downloads/gophrier/gophrier-0.2.3/CMakeFiles/gophrier.dir/linux/hand.c.o"
  "/home/tastyfish/Downloads/gophrier/gophrier-0.2.3/list.c" "/home/tastyfish/Downloads/gophrier/gophrier-0.2.3/CMakeFiles/gophrier.dir/list.c.o"
  "/home/tastyfish/Downloads/gophrier/gophrier-0.2.3/log.c" "/home/tastyfish/Downloads/gophrier/gophrier-0.2.3/CMakeFiles/gophrier.dir/log.c.o"
  "/home/tastyfish/Downloads/gophrier/gophrier-0.2.3/main.c" "/home/tastyfish/Downloads/gophrier/gophrier-0.2.3/CMakeFiles/gophrier.dir/main.c.o"
  "/home/tastyfish/Downloads/gophrier/gophrier-0.2.3/map.c" "/home/tastyfish/Downloads/gophrier/gophrier-0.2.3/CMakeFiles/gophrier.dir/map.c.o"
  "/home/tastyfish/Downloads/gophrier/gophrier-0.2.3/path.c" "/home/tastyfish/Downloads/gophrier/gophrier-0.2.3/CMakeFiles/gophrier.dir/path.c.o"
  "/home/tastyfish/Downloads/gophrier/gophrier-0.2.3/pool.c" "/home/tastyfish/Downloads/gophrier/gophrier-0.2.3/CMakeFiles/gophrier.dir/pool.c.o"
  "/home/tastyfish/Downloads/gophrier/gophrier-0.2.3/prerequest.c" "/home/tastyfish/Downloads/gophrier/gophrier-0.2.3/CMakeFiles/gophrier.dir/prerequest.c.o"
  "/home/tastyfish/Downloads/gophrier/gophrier-0.2.3/request.c" "/home/tastyfish/Downloads/gophrier/gophrier-0.2.3/CMakeFiles/gophrier.dir/request.c.o"
  "/home/tastyfish/Downloads/gophrier/gophrier-0.2.3/server.c" "/home/tastyfish/Downloads/gophrier/gophrier-0.2.3/CMakeFiles/gophrier.dir/server.c.o"
  "/home/tastyfish/Downloads/gophrier/gophrier-0.2.3/sock.c" "/home/tastyfish/Downloads/gophrier/gophrier-0.2.3/CMakeFiles/gophrier.dir/sock.c.o"
  "/home/tastyfish/Downloads/gophrier/gophrier-0.2.3/type.c" "/home/tastyfish/Downloads/gophrier/gophrier-0.2.3/CMakeFiles/gophrier.dir/type.c.o"
  )
set(CMAKE_C_COMPILER_ID "GNU")

# Preprocessor definitions for this target.
set(CMAKE_TARGET_DEFINITIONS_C
  "GOPHRIER_CONF_DIR=\"/etc/gophrier\""
  "GOPHRIER_DEFAULT_BROWSABLE=1"
  "GOPHRIER_DEFAULT_DAEMON=1"
  "GOPHRIER_DEFAULT_GM_ENABLE=1"
  "GOPHRIER_DEFAULT_GM_NAME=\"gophermap\""
  "GOPHRIER_DEFAULT_IPV6=0"
  "GOPHRIER_DEFAULT_LOG_DIR=\"/var/log/gophrier\""
  "GOPHRIER_DEFAULT_PORT=70"
  "GOPHRIER_DEFAULT_ROOT_DIR=\"/var/gopher\""
  "GOPHRIER_DEFAULT_SHOW_HIDDEN=0"
  "GOPHRIER_DEFAULT_SYMLINKS=1"
  "GOPHRIER_DEFAULT_TYPE_HELPER=\"ext\""
  "GOPHRIER_MOD_DIR=\"/usr/local/lib/gophrier\""
  "GOPHRIER_MOD_ENABLED=0"
  "GOPHRIER_PID_PATH=\"/var/run/gophrier.pid\""
  "GOPHRIER_VERSION=\"0.2.3\""
  "_GNU_SOURCE"
  )

# The include file search paths:
set(CMAKE_C_TARGET_INCLUDE_PATH
  )

# Targets to which this target links.
set(CMAKE_TARGET_LINKED_INFO_FILES
  )

# Fortran module output directory.
set(CMAKE_Fortran_TARGET_MODULE_DIR "")
