/*  Copyright 2010-2011 Guillaume Duhamel

    This file is part of Gophrier.

    Gophrier is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    Gophrier is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Gophrier; if not, write to the Free Software
    Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301  USA
*/

#ifndef GOPHRIER_REQUEST_H
#define GOPHRIER_REQUEST_H

#include "core.h"

#include <sys/types.h>

struct _Request
{
    Socket * sock;
    Path * fullpath;
    Path * path;
    char * query;
    char * sockname;
    char * sockpeer;
};

Request * request_new(Socket * sock, const char * path);
void request_exec(Request * request);
void request_free(Request * request);
ssize_t request_send(Request * request, const void * buf, size_t len);

void request_push(Request * request, char type, const char * name, const char * path, const char * host, unsigned int port);
void request_close(Request * request);

#endif
