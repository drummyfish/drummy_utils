/*  Copyright 2011 Guillaume Duhamel

    This file is part of Gophrier.

    Gophrier is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    Gophrier is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Gophrier; if not, write to the Free Software
    Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301  USA
*/

#include "type.h"
#include "conf.h"
#if GOPHRIER_MOD_ENABLED
#include "mod.h"
#endif

#include <stdlib.h>
#include <string.h>

typedef char (*TypeHelper)(const char * path);

static TypeHelper type_helper = NULL;

static void type_init();
static char dummyhelper(const char * path);
static char exthelper(const char * path);

char type_guess(const char * path)
{
    if (NULL == type_helper)
        type_init();

    return type_helper(path);
}

static void type_init()
{
    char * helpername = conf_get_type_helper();

    if (! strcmp("ext", helpername))
        type_helper = exthelper;
    else
    {
#if GOPHRIER_MOD_ENABLED
        type_helper = mod_load(helpername, MOD_TYPE_HELPER);
        if (! type_helper)
            type_helper = dummyhelper;
#else
        type_helper = dummyhelper;
#endif
    }
}

static char dummyhelper(__attribute ((unused)) const char * path)
{
    return 0;
}

static char exthelper(const char * path)
{
    char * ext;

    ext = strrchr(path, '.');
    if (ext)
    {
        if (! strcasecmp(ext, ".html"))
            return 'h';
        else if (
            (! strcasecmp(ext, ".gif")) ||
            (! strcasecmp(ext, ".png")) ||
            (! strcasecmp(ext, ".jpg")))
            return 'I';
        else if (
            (! strcasecmp(ext, ".7z")) ||
            (! strcasecmp(ext, ".bz2")) ||
            (! strcasecmp(ext, ".gzip")) ||
            (! strcasecmp(ext, ".rar")) ||
            (! strcasecmp(ext, ".zip")))
            return '9';
        else if (
            (! strcasecmp(ext, ".txt")) ||
            (! strcasecmp(ext, ".c")) ||
            (! strcasecmp(ext, ".h")) ||
            (! strcasecmp(ext, ".md")))
            return '0';
    }
    return 0;
}
