/*  Copyright 2011 Guillaume Duhamel

    This file is part of Gophrier.

    Gophrier is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    Gophrier is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Gophrier; if not, write to the Free Software
    Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301  USA
*/

#ifndef GOPHRIER_PREREQUEST_H
#define GOPHRIER_PREREQUEST_H

#include "core.h"

struct _PreRequest
{
    char * path;
    unsigned int pathsize;
    int ok;
    unsigned int pos;
};

PreRequest * pre_request_new();
void pre_request_free(PreRequest * pr);
void pre_request_set_valid(PreRequest * pr, int valid);

#endif
