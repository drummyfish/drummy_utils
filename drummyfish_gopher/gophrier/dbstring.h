/*  Copyright 2010-2011 Guillaume Duhamel

    This file is part of Gophrier.

    Gophrier is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    Gophrier is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Gophrier; if not, write to the Free Software
    Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301  USA
*/

#ifndef GOPHRIER_DBSTRING_H
#define GOPHRIER_DBSTRING_H

#include "core.h"

struct _DBString
{
    char * buffer[2];
    int front;
    int back;
};

DBString * dbstring_new(int buflen);
void dbstring_free(DBString * string);
void dbstring_prepend(DBString * string, const char * toprepend);
void dbstring_shift(DBString * string, int toshift);
char * dbstring_content(DBString * string);

#endif
