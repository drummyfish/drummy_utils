#include <magic.h>
#include <stdio.h>
#include <string.h>

static magic_t cookie;

int mod_init()
{
    cookie = magic_open(MAGIC_MIME_TYPE);
    magic_load(cookie, NULL);

    return 0;
}

char type_guess(const char * path)
{
    const char * type;
    int typelen;
    char ret = 0;

    type = magic_file(cookie, path);
    if (NULL == type) return 0;

    typelen = strlen(type);
    if ((typelen > 6) && (0 == strncmp("image/", type, 6)))
        ret = 'I';
    else if ((typelen > 5) && (0 == strncmp("text/", type, 5)))
        ret = '0';
    else if ((typelen > 12) && (0 == strncmp("application/", type, 12)))
        ret = '9';

    return ret;
}
