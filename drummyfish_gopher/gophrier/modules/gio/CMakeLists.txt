project(gio_mod)

include(FindPkgConfig)
pkg_check_modules(GIO gio-2.0)
if(NOT GIO_FOUND)
    return()
endif()

set(gio_mod_SOURCES gio_mod.c)

include_directories(${GIO_INCLUDE_DIRS})

add_library(giomod MODULE ${gio_mod_SOURCES})
target_link_libraries(giomod ${GIO_LDFLAGS})

install(TARGETS giomod DESTINATION ${GOPHRIER_MOD_DIR})
