#include <gio/gio.h>

int mod_init()
{
    g_type_init();

    return 0;
}

char type_guess(const char * path)
{
    char * type;
    char ret = 0;

    type = g_content_type_guess(path, NULL, 0, NULL);
    if (g_content_type_is_a(type, "image/*")) {
        ret = 'I';
    } else if (g_content_type_is_a(type, "text/*")) {
        ret = '0';
    } else if (g_content_type_is_a(type, "application/*")) {
        ret = '9';
    }

    g_free(type);

    return ret;
}
