/*  Copyright 2010-2011 Guillaume Duhamel

    This file is part of Gophrier.

    Gophrier is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    Gophrier is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Gophrier; if not, write to the Free Software
    Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301  USA
*/

#include "cli.h"
#include "conf.h"

#include <stdarg.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

static void cli_version(void)
{
    fprintf(stderr, "gophrier " GOPHRIER_VERSION "\n");
    exit(0);
}

static void cli_usage(void)
{
    fprintf(stderr, "usage: gophrier [OPTION]\n");
    fprintf(stderr, " -4             run in ipv4 mode\n");
    fprintf(stderr, " -6             run in ipv6 mode\n");
    fprintf(stderr, " -b 0|1         browse directories (1) or not (0)\n");
    fprintf(stderr, " -c PATH        read gophrier configuration in the specified PATH\n");
    fprintf(stderr, " -d 0|1         run as a daemon (1) or as a normal process (0)\n");
    fprintf(stderr, " -g 0|1         use gophermap files (1) or not (0)\n");
    fprintf(stderr, " --group GROUP  drop root group and use gid of the specified GROUP\n");
    fprintf(stderr, " --host HOST    host that will be used in gopher selectors\n");
    fprintf(stderr, " -l PATH        output log files in the specified PATH\n");
#if GOPHRIER_MOD_ENABLED
    fprintf(stderr, " -m PATH        load modules in the specified PATH\n");
#endif
    fprintf(stderr, " --pidfile PATH wrote pid to PATH\n");
    fprintf(stderr, " -p PORT        listen on PORT\n");
    fprintf(stderr, " -r PATH        use PATH as the root directory\n");
    fprintf(stderr, " -s 0|1         display symlinks (1) or not (0)\n");
    fprintf(stderr, " --user USER    drop root privileges and use uid/gid of the specified USER\n");
    fprintf(stderr, " -h             display this help and exit\n");
    fprintf(stderr, " -v             display version and exit\n");
    exit(6);
}

static void cli_error(const char * format, ...)
{
    va_list l;

    va_start(l, format);
    vfprintf(stderr, format, l);
    va_end(l);
    exit(6);
}

static void cli_browse(int * argc, char *** argv)
{
    (*argv)++;
    (*argc)--;
    if (*argc)
    {
        char * end;
        int value = strtol(**argv, &end, 10);
        if (end == **argv + strlen(**argv))
            conf_set_browsable(value);
        else
            cli_error("%s is not a valid value for browsable\n", **argv);
    }
    else
        cli_error("missing browsable parameter\n");
}

static void cli_conf(int * argc, char *** argv)
{
    (*argv)++;
    (*argc)--;
    if (*argc)
        conf_set_conf_dir(**argv);
    else
        cli_error("missing conf parameter\n");
}

static void cli_daemon(int * argc, char *** argv)
{
    (*argv)++;
    (*argc)--;
    if (*argc)
    {
        char * end;
        int value = strtol(**argv, &end, 10);
        if (end == **argv + strlen(**argv))
            conf_set_daemon(value);
        else
            cli_error("%s is not a valid value for port\n", **argv);
    }
    else
        cli_error("missing daemon parameter\n");
}

static void cli_gm_enable(int * argc, char *** argv)
{
    (*argv)++;
    (*argc)--;
    if (*argc)
    {
        char * end;
        int value = strtol(**argv, &end, 10);
        if (end == **argv + strlen(**argv))
            conf_set_gm_enable(value);
        else
            cli_error("%s is not a valid value for gophermap\n", **argv);
    }
    else
        cli_error("missing gophermap parameter\n");
}

static void cli_group(int * argc, char *** argv)
{
    (*argv)++;
    (*argc)--;
    if (*argc)
        conf_set_group(**argv);
}

static void cli_host(int * argc, char *** argv)
{
    (*argv)++;
    (*argc)--;
    if (*argc)
        conf_set_host(**argv);
}

static void cli_ipv4(void)
{
    conf_set_ipv6(0);
}

static void cli_ipv6(void)
{
    conf_set_ipv6(1);
}

static void cli_log(int * argc, char *** argv)
{
    (*argv)++;
    (*argc)--;
    if (*argc)
        conf_set_log_dir(**argv);
    else
        cli_error("missing log parameter\n");
}

#if GOPHRIER_MOD_ENABLED
static void cli_mod(int * argc, char *** argv)
{
    (*argv)++;
    (*argc)--;
    if (*argc)
        conf_set_mod_dir(**argv);
    else
        cli_error("missing mod parameter\n");
}
#endif

static void cli_pidfile(int * argc, char *** argv)
{
    (*argv)++;
    (*argc)--;
    if (*argc)
        conf_set_pid_path(**argv);
    else
        cli_error("missing pidfile parameter\n");
}

static void cli_port(int * argc, char *** argv)
{
    (*argv)++;
    (*argc)--;
    if (*argc)
    {
        char * end;
        int value = strtol(**argv, &end, 10);
        if (end == **argv + strlen(**argv))
            conf_set_port(value);
        else
            cli_error("%s is not a valid value for port\n", **argv);
    }
    else
        cli_error("missing port parameter\n");
}

static void cli_root(int * argc, char *** argv)
{
    (*argv)++;
    (*argc)--;
    if (*argc)
        conf_set_root_dir(**argv);
    else
        cli_error("missing root parameter\n");
}

static void cli_symlinks(int * argc, char *** argv)
{
    (*argv)++;
    (*argc)--;
    if (*argc)
    {
        char * end;
        int value = strtol(**argv, &end, 10);
        if (end == **argv + strlen(**argv))
            conf_set_symlinks(value);
        else
            cli_error("%s is not a valid value for symlinks\n", **argv);
    }
    else
        cli_error("missing symlinks parameter\n");
}

static void cli_user(int * argc, char *** argv)
{
    (*argv)++;
    (*argc)--;
    if (*argc)
        conf_set_user(**argv);
}

void cli_parse(int argc, char ** argv)
{
    argv++;
    argc--;
    while(argc)
    {
        if (! strcmp("-4", *argv)) cli_ipv4();
        else if (! strcmp("-6", *argv)) cli_ipv6();
        else if (! strcmp("-b", *argv)) cli_browse(&argc, &argv);
        else if (! strcmp("-c", *argv)) cli_conf(&argc, &argv);
        else if (! strcmp("-d", *argv)) cli_daemon(&argc, &argv);
        else if (! strcmp("-g", *argv)) cli_gm_enable(&argc, &argv);
        else if (! strcmp("--group", *argv)) cli_group(&argc, &argv);
        else if (! strcmp("-h", *argv)) cli_usage();
        else if (! strcmp("--host", *argv)) cli_host(&argc, &argv);
        else if (! strcmp("-l", *argv)) cli_log(&argc, &argv);
#if GOPHRIER_MOD_ENABLED
        else if (! strcmp("-m", *argv)) cli_mod(&argc, &argv);
#endif
        else if (! strcmp("--pidfile", *argv)) cli_pidfile(&argc, &argv);
        else if (! strcmp("-p", *argv)) cli_port(&argc, &argv);
        else if (! strcmp("-r", *argv)) cli_root(&argc, &argv);
        else if (! strcmp("-s", *argv)) cli_symlinks(&argc, &argv);
        else if (! strcmp("--user", *argv)) cli_user(&argc, &argv);
        else if (! strcmp("-v", *argv)) cli_version();
        else cli_error("invalid option: %s\n", *argv);
        argv++;
        argc--;
    }
}
