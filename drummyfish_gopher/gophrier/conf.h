/*  Copyright 2010-2011 Guillaume Duhamel

    This file is part of Gophrier.

    Gophrier is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    Gophrier is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Gophrier; if not, write to the Free Software
    Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301  USA
*/

#ifndef GOPHRIER_CONF_H
#define GOPHRIER_CONF_H

#include "list.h"

void conf_reset();

char * conf_get_conf_dir();
int    conf_set_conf_dir(const char * path);
char * conf_get_root_dir();
int    conf_set_root_dir(const char * path);
char * conf_get_log_dir();
int    conf_set_log_dir(const char * path);
#if GOPHRIER_MOD_ENABLED
char * conf_get_mod_dir();
int    conf_set_mod_dir(const char * path);
#endif
char * conf_get_pid_path();
int    conf_set_pid_path(const char * path);
int conf_is_daemon();
int conf_set_daemon(int d);
int conf_use_ipv6();
int conf_set_ipv6(int i);
int conf_get_port();
int conf_set_port(int p);
int conf_get_browsable();
int conf_set_browsable(int enable);
char * conf_get_type_helper();
int conf_get_symlinks();
int conf_set_symlinks(int enable);
int conf_get_hidden();
int conf_set_hidden(int enable);
char * conf_get_user();
int conf_set_user(const char * user);
char * conf_get_group();
int conf_set_group(const char * user);
char * conf_get_host();
int conf_set_host(const char * host);

/* Gophermap options */
int    conf_get_gm_enable();
int    conf_set_gm_enable(int enable);
List * conf_get_gm_names();

#endif
