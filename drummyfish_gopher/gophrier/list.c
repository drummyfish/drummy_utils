/*  Copyright 2011 Guillaume Duhamel

    This file is part of Gophrier.

    Gophrier is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    Gophrier is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Gophrier; if not, write to the Free Software
    Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301  USA
*/

#include "list.h"

#include <stdio.h>
#include <stdlib.h>

typedef struct {
    List * first;
    List * last;
} ListMeta;

static ListMeta * list_meta_new()
{
    ListMeta * meta;

    meta = (ListMeta *) malloc(sizeof(ListMeta));
    meta->first = NULL;
    meta->last = NULL;

    return meta;
}

static void list_meta_free(ListMeta * meta)
{
    free(meta);
}

static List * list_meta_push(ListMeta * meta, List * list)
{
    list->meta = meta;

    if (NULL == meta->first)
        meta->first = list;

    if (NULL != meta->last)
        meta->last->next = list;
    meta->last = list;

    return meta->first;
}

static List * list_meta_pop(ListMeta * meta, List ** list)
{
    if (meta->first == meta->last)
    {
        *list = meta->first;
        return NULL;
    }
    else
    {
        List * prev;
        prev = meta->first;
        while(prev->next != meta->last)
            prev = prev->next;

        *list = meta->last;
        meta->last = prev;
        prev->next = NULL;
        return meta->first;
    }
}

static List * list_new(void * elem)
{
    List * list;

    list = (List *) malloc(sizeof(List));
    list->current = elem;
    list->next = NULL;
    list->meta = NULL;

    return list;
}

void list_free(List * list)
{
    if (NULL == list)
        return;

    if (list->next)
        list_free(list->next);
    else if (list->meta)
        list_meta_free(list->meta);

    free(list->current);
    free(list);
}

List * list_push(List * list, void * elem)
{
    List * newlist;
    ListMeta * meta;

    meta = (list == NULL) ? list_meta_new() : list->meta;
    newlist = list_new(elem);
    return list_meta_push(meta, newlist);
}

List * list_pop(List * list, void ** elem)
{
    if (NULL == list)
    {
        *elem = NULL;
        return NULL;
    }

    List * removed, * returned;
    returned = list_meta_pop(list->meta, &removed);
    if (NULL == returned)
        list_meta_free(list->meta);
    *elem = removed->current;
    free(removed);
    return returned;
}
