#include "mod.h"
#include "conf.h"
#include "path.h"

#include <stdio.h>
#include <dlfcn.h>
#include <stdlib.h>

typedef int (*ModInit)();

void * mod_load(const char * name, mod_type type)
{
    const char * moddir = conf_get_mod_dir();
    Path * absname;
    void * mod;
    ModInit init;

    absname = path_new(name, moddir);
    mod = dlopen(absname->path, RTLD_LAZY);
    path_free(absname);
    if (NULL == mod)
    {
        fprintf(stderr, "Warning: failed to load module %s: %s\n", name, dlerror());
        return NULL;
    }

    switch(type)
    {
        case MOD_TYPE_HELPER:
            init = dlsym(mod, "mod_init");
            if (init() != 0)
                return NULL;
            return dlsym(mod, "type_guess");
            break;
        default:
            return NULL;
    }

    return NULL;
}
