/*  Copyright 2011 Guillaume Duhamel

    This file is part of Gophrier.

    Gophrier is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    Gophrier is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Gophrier; if not, write to the Free Software
    Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301  USA
*/

#include "prerequest.h"

#include <stdlib.h>

PreRequest * pre_request_new()
{
    PreRequest * pr;

    pr = (PreRequest *) malloc(sizeof(PreRequest));
    pr->path = NULL;
    pr->pathsize = 0;
    pr->ok = 0;
    pr->pos = 0;

    return pr;
}

void pre_request_free(PreRequest * pr)
{
    free(pr->path);
    free(pr);
}

void pre_request_set_valid(PreRequest * pr, int valid)
{
    if (! valid)
    {
        free(pr->path);
        pr->path = NULL;
    }
    if (pr->path != NULL)
    {
        if (pr->pos >= pr->pathsize)
            pr->path = realloc(pr->path, pr->pos + 1);
        pr->path[pr->pos] = 0;
    }
    pr->ok = 1;
}
