/*  Copyright 2010-2011 Guillaume Duhamel

    This file is part of Gophrier.

    Gophrier is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    Gophrier is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Gophrier; if not, write to the Free Software
    Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301  USA
*/

#include "request.h"
#include "conf.h"
#include "dir.h"
#include "file.h"
#include "error.h"
#include "exe.h"
#include "map.h"
#include "sock.h"
#include "path.h"

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/stat.h>
#include <sys/socket.h>
#include <netdb.h>
#include <unistd.h>

static Path * request_path_cleanup(const char * inpath, char ** query);
static char * request_query_decode(const char * query);

Request * request_new(Socket * sock, const char * path)
{
    Request * request;

    request = (Request *) malloc(sizeof(Request));
    request->sock = sock;

    request->path = request_path_cleanup(path, &request->query);
    request->fullpath = path_new(request->path->path, conf_get_root_dir());

    {
        struct sockaddr * myaddr;
        socklen_t size;
        char host[1024];

        if (conf_use_ipv6())
            size = sizeof(struct sockaddr_in6);
        else
            size = sizeof(struct sockaddr_in);
        myaddr = malloc(size);

        {
            const char * confhost = conf_get_host();
            if (*confhost != '\0')
                request->sockname = strdup(confhost);
            else
            {
                getsockname(sock->fd, myaddr, &size);
                getnameinfo(myaddr, size, host, 1024, NULL, 0, NI_NUMERICHOST);
                request->sockname = strdup(host);
            }
        }

        getpeername(sock->fd, myaddr, &size);
        getnameinfo(myaddr, size, host, 1024, NULL, 0, NI_NUMERICHOST);
        request->sockpeer = strdup(host);

        free(myaddr);
    }

    return request;
}

void request_exec(Request * request)
{
    if (! request->fullpath->valid)
        return error_parse(request);
    if (path_is_link(request->fullpath) && (! conf_get_symlinks()))
        return error_parse(request);
    if (path_is_dir(request->fullpath))
    {
        Path * gmpath = map_path(request->fullpath);
        if (NULL == gmpath)
        {
            if (conf_get_browsable())
                dir_parse(request);
            else
                error_parse(request);
        }
        else
        {
            path_free(request->fullpath);
            request->fullpath = gmpath;
            map_parse(request);
        }
    }
    else if (path_is_executable(request->fullpath))
        exe_parse(request);
    else
        file_parse(request);
}

void request_free(Request * request)
{
    path_free(request->fullpath);
    path_free(request->path);
    free(request->query);
    free(request->sockname);
    free(request->sockpeer);
    free(request);
}

static Path * request_path_cleanup(const char * inpath, char ** query)
{
    char * path = strdup(inpath);
    Path * outpath;
    char * querymark;

    querymark = strrchr(path, '?');
    if (querymark)
    {
        *query = request_query_decode(querymark + 1);
        *querymark = 0;
    }
    else
        *query = NULL;

    outpath = path_new(path, "/");
    free(path);
    return outpath;
}

ssize_t request_send(Request * request, const void * buf, size_t len)
{
    return send(request->sock->fd, buf, len, MSG_NOSIGNAL);
}

static char * request_query_decode(const char * query)
{
    char * decoded = malloc(strlen(query) + 1);
    const char * c = query;
    int i = 0;

    while(*c)
    {
        if (*c == '%')
        {
            unsigned int code;

            sscanf(c + 1, "%2X", &code);
            decoded[i] = code;
            c += 2;
        }
        else
            decoded[i] = *c;
        c++;
        i++;
    }
    decoded[i] = 0;

    return decoded;
}

void request_push(Request * request, char type, const char * name, const char * path, const char * host, unsigned int port)
{
    char buffer[1024], tab;
    tab = '\t';

    request_send(request, &type, 1);
    request_send(request, name, strlen(name));
    request_send(request, &tab, 1);
    request_send(request, path, strlen(path));
    request_send(request, &tab, 1);
    request_send(request, host, strlen(host));
    request_send(request, &tab, 1);

    sprintf(buffer, "%u\r\n", port);
    request_send(request, buffer, strlen(buffer));
}

void request_close(Request * request)
{
    request_send(request, ".\r\n", 3);
    close(request->sock->fd);
}
