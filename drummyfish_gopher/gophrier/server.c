/*  Copyright 2010-2011 Guillaume Duhamel

    This file is part of Gophrier.

    Gophrier is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    Gophrier is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Gophrier; if not, write to the Free Software
    Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301  USA
*/

#include "server.h"
#include "pool.h"
#include "client.h"
#include "conf.h"

#include <errno.h>
#include <sys/types.h>
#include <pwd.h>
#include <grp.h>
#include <unistd.h>

Socket * server_socket_new()
{
    int serverSocket;
    int opt = 1;
    Socket * sock;

    if (conf_use_ipv6())
    {
        struct sockaddr_in6 addr;

        serverSocket = socket(AF_INET6, SOCK_STREAM, 0);

        if (setsockopt(serverSocket, SOL_SOCKET, SO_REUSEADDR, &opt, sizeof(int)) == -1)
        {
            perror("setsockopt");
            exit(1);
        }

        memset(&addr, 0, sizeof(struct sockaddr_in6));
        addr.sin6_family = AF_INET6;
        addr.sin6_addr = in6addr_any;
        addr.sin6_port = htons(conf_get_port());
        if (bind(serverSocket, (struct sockaddr *) &addr, sizeof(addr)) == -1)
        {
            perror("bind");
            exit(2);
        }
    }
    else
    {
        struct sockaddr_in addr;

        serverSocket = socket(AF_INET, SOCK_STREAM, 0);

        if (setsockopt(serverSocket, SOL_SOCKET, SO_REUSEADDR, &opt, sizeof(int)) == -1)
        {
            perror("setsockopt");
            exit(1);
        }

        memset(&addr, 0, sizeof(struct sockaddr_in));
        addr.sin_family = AF_INET;
        addr.sin_addr.s_addr = INADDR_ANY;
        addr.sin_port = htons(conf_get_port());
        if (bind(serverSocket, (struct sockaddr *) &addr, sizeof(addr)) == -1)
        {
            fprintf(stderr, "Can't bind to port %d: %s\n", conf_get_port(), strerror(errno));
            exit(2);
        }
    }

    if (listen(serverSocket, 3) == -1)
    {
        perror("listen");
        exit(3);
    }

    {
        const char * user, * group;
        int group_have_been_set = 0;

        group = conf_get_group();
        if (*group != '\0')
        {
            struct group * g;
            g = getgrnam(group);
            if (g)
                if (0 == setgid(g->gr_gid))
                    group_have_been_set = 1;
        }

        user = conf_get_user();
        if (*user != '\0')
        {
            struct passwd * p;
            p = getpwnam(user);
            if (p)
            {
                setuid(p->pw_uid);
                if (! group_have_been_set) setgid(p->pw_gid);
            }
        }
    }

    sock = socket_new(serverSocket);
    sock->onRead = server_socket_on_read;

    return sock;
}

void server_socket_on_read(Socket * sock, SocketPool * pool)
{
    int clientSocket;
    clientSocket = accept(sock->fd, NULL, NULL);
    if (clientSocket == -1)
    {
       perror("accept");
       exit(5);
    }
    socket_pool_add(pool, client_socket_new(clientSocket));
}
