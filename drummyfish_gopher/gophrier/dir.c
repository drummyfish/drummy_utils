/*  Copyright 2010-2011 Guillaume Duhamel

    This file is part of Gophrier.

    Gophrier is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    Gophrier is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Gophrier; if not, write to the Free Software
    Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301  USA
*/

#include "dir.h"
#include "sock.h"
#include "request.h"
#include "conf.h"
#include "path.h"
#include "type.h"

#include <stdlib.h>
#include <string.h>
#include <dirent.h>
#include <fcntl.h>
#include <stdio.h>
#include <unistd.h>

static int dir_sort(const struct dirent ** a, const struct dirent ** b)
{
#ifdef _DIRENT_HAVE_D_TYPE
    unsigned char ida, idb;
    ida = (*a)->d_type == DT_DIR;
    idb = (*b)->d_type == DT_DIR;
    if (ida != idb)
    {
        if (ida) return -1;
        return 1;
    }
#endif
    return strcasecmp((*a)->d_name, (*b)->d_name);
}

void dir_parse(Request * request)
{
    int i, n;
    struct dirent ** namelist, * current;

    n = scandir(request->fullpath->path, &namelist, NULL, dir_sort);
    if (-1 == n)
    {
        request_close(request);
        return;
    }

    for(i = 0;i < n;i++)
    {
        Path * absname, * fullname;
        current = namelist[i];
        if ((0 == conf_get_hidden()) && (current->d_name[0] == '.'))
        {
            free(current);
            continue;
        }

        absname = path_new(current->d_name, request->path->path);
        fullname = path_new(absname->path, conf_get_root_dir());
        if (path_is_link(fullname) && (! conf_get_symlinks()))
            ;
        else if (path_is_dir(fullname))
            request_push(request, '1', current->d_name, absname->path, request->sockname, conf_get_port());
        else if (path_is_regular(fullname))
        {
            char type = type_guess(fullname->path);
            if (type)
                request_push(request, type, current->d_name, absname->path, request->sockname, conf_get_port());
            else
                request_push(request, '9', current->d_name, absname->path, request->sockname, conf_get_port());
        }
        else
            request_push(request, 'i', current->d_name, "fake", "(NULL)", 0);

        path_free(absname);
        path_free(fullname);
        free(current);
    }
    free(namelist);

    request_close(request);
}
