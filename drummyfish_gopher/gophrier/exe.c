/*  Copyright 2010-2011 Guillaume Duhamel

    This file is part of Gophrier.

    Gophrier is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    Gophrier is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Gophrier; if not, write to the Free Software
    Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301  USA
*/

#include "file.h"
#include "sock.h"
#include "request.h"
#include "path.h"

#include <stdlib.h>
#include <string.h>
#include <fcntl.h>
#include <unistd.h>
#include <sys/stat.h>
#include <sys/wait.h>
#include <libgen.h>
#include <pwd.h>
#include <errno.h>

void exe_parse(Request * request)
{
    char buffer[1024];
    int len;
    int pipefd[2];
    pid_t pid;
    struct stat info;

    stat(request->fullpath->path, &info);

    pipe(pipefd);
    
    pid = fork();
    if (pid == -1)
    {
        close(pipefd[0]);
        close(pipefd[1]);
        close(request->sock->fd);
        return;
    }
    
    if (pid == 0)
    {
        close(pipefd[0]);
        close(1);
        dup(pipefd[1]);

        clearenv();
        setenv("PATH", "/usr/bin:/bin", 1);

        if (request->query != NULL)
            setenv("SEARCHREQUEST", request->query, 1);

        setuid(info.st_uid);

        {
            struct passwd * user;
            user = getpwuid(info.st_uid);
            setenv("LOGNAME", user->pw_name, 1);
            setenv("HOME", user->pw_dir, 1);
        }

        {
            char * pathcopy = strdup(request->fullpath->path);
            char * dir = dirname(pathcopy);
            chdir(dir);
            setenv("PWD", dir, 1);
            free(pathcopy);
        }

        setenv("REMOTE_ADDR", request->sockpeer, 1);

        execl(request->fullpath->path, request->fullpath->path, NULL);

        /* If we're reaching this, exec has failed, trying something else... */
        if (errno == ENOEXEC)
        {
            /* Ok, fullpath was not executable, we're sending the file content
             * to the client and we should append something in the error log.
             */
            int fd;
            char buffer[1024];
            int len;

            fd = open(request->fullpath->path, O_RDONLY);
            while((len = read(fd, buffer, 1024)) > 0)
                write(1, buffer, len);
            close(fd);
        }
        exit(0);
    }
    else
    {
        close(pipefd[1]);
        while((len = read(pipefd[0], buffer, 1024)) > 0)
            request_send(request, buffer, len);
        close(pipefd[0]);
        close(request->sock->fd);
        waitpid(pid, NULL, 0);
    }
}
