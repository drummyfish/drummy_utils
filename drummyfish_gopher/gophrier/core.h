/*  Copyright 2010-2011 Guillaume Duhamel

    This file is part of Gophrier.

    Gophrier is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    Gophrier is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Gophrier; if not, write to the Free Software
    Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301  USA
*/

#ifndef GOPHRIER_CORE_H
#define GOPHRIER_CORE_H

#if GOPHRIER_POSIX
#include "posix/compat.h"
#endif

typedef struct _List List;

typedef struct _Socket Socket;
typedef struct _SocketPool SocketPool;
typedef struct _Request Request;
typedef struct _DBString DBString;
typedef struct _PreRequest PreRequest;
typedef struct _Path Path;

#endif
