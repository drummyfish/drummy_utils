/*  Copyright 2010-2011 Guillaume Duhamel

    This file is part of Gophrier.

    Gophrier is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    Gophrier is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Gophrier; if not, write to the Free Software
    Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301  USA
*/

#include <sys/socket.h>
#include <netinet/in.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <sys/select.h>
#include <errno.h>

#include "pool.h"
#include "conf.h"
#include "cli.h"

SocketPool * socket_pool_new(Socket * s, int argc, char ** argv)
{
    SocketPool * pool;

    pool = (SocketPool *) malloc(sizeof(SocketPool));
    pool->maxSocket = s->fd;
    pool->sockets = (Socket **) malloc(sizeof(Socket *) * (pool->maxSocket + 1));
    memset(pool->sockets, 0, sizeof(Socket *) * (pool->maxSocket + 1));
    pool->sockets[s->fd] = s;
    pool->running = 1;
    pool->argc = argc;
    pool->argv = argv;

    return pool;
}

void socket_pool_free(SocketPool * pool)
{
    int i;

    for(i = 0;i < pool->maxSocket + 1;i++)
        if(pool->sockets[i])
            socket_free(pool->sockets[i]);
    free(pool->sockets);
    free(pool);
}

void socket_pool_add(SocketPool * pool, Socket * s)
{
    if (s->fd > pool->maxSocket)
    {
        int i, oldMax = pool->maxSocket;
        pool->maxSocket = s->fd;
        pool->sockets = (Socket **) realloc(pool->sockets, sizeof(Socket *) * (pool->maxSocket + 1));
        for(i = oldMax + 1;i < pool->maxSocket + 1;i++)
            pool->sockets[i] = NULL;
    }

    pool->sockets[s->fd] = s;
}

void socket_pool_remove(SocketPool * pool, Socket * s)
{
    pool->sockets[s->fd] = NULL;
    socket_free(s);
}

void socket_pool_fill_fd_set(SocketPool * pool, fd_set * fs)
{
    int i;

    FD_ZERO(fs);
    for(i = 0;i < pool->maxSocket + 1;i++)
        if(pool->sockets[i])
            FD_SET(i, fs);
}

void socket_pool_run(SocketPool * pool)
{
    fd_set fs;
    int readsocks;

    socket_pool_fill_fd_set(pool, &fs);
    
    readsocks = select(pool->maxSocket + 1, &fs, NULL, NULL, NULL);
    if (readsocks < 0)
    {
	if (errno == EINTR)
            return;
        perror("select");
        exit(4);
    }
    if (readsocks == 0)
    {
        // nothing to read... hu?
    }
    else
    {
        int i;
        for(i = 0;i < pool->maxSocket + 1;i++)
            if (FD_ISSET(i, &fs))
                pool->sockets[i]->onRead(pool->sockets[i], pool);
    }
}

int socket_pool_start(SocketPool * pool)
{
    while(pool->running)
        socket_pool_run(pool);

    socket_pool_free(pool);

    return 0;
}

void socket_pool_stop(SocketPool * pool)
{
    pool->running = 0;
}

void socket_pool_reload(SocketPool * pool)
{
    conf_reset();
    cli_parse(pool->argc, pool->argv);
}
