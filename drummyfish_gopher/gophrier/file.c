/*  Copyright 2010-2011 Guillaume Duhamel

    This file is part of Gophrier.

    Gophrier is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    Gophrier is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Gophrier; if not, write to the Free Software
    Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301  USA
*/

#include "file.h"
#include "sock.h"
#include "request.h"
#include "path.h"

#include <stdlib.h>
#include <string.h>
#include <fcntl.h>
#include <unistd.h>
#include <pthread.h>

typedef struct {
    int fd;
    char * path;
} FileRequest;

static FileRequest * file_request_new(Request * request)
{
    FileRequest * fr;
    fr = (FileRequest *) malloc(sizeof(FileRequest));
    fr->fd = request->sock->fd;
    fr->path = strdup(request->fullpath->path);
    return fr;
}

static void file_request_free(FileRequest * fr)
{
    free(fr->path);
    free(fr);
}

static void * file_async_parse(void * fr)
{
    int fd;
    char buffer[1024];
    int len;
    FileRequest * request = fr;

    fd = open(request->path, O_RDONLY);
    while((len = read(fd, buffer, 1024)) > 0)
        send(request->fd, buffer, len, MSG_NOSIGNAL);
    close(fd);
    close(request->fd);

    file_request_free(request);

    return NULL;
}

void file_parse(Request * request)
{
    pthread_t async;
    FileRequest * fr;

    fr = file_request_new(request);
    pthread_create(&async, NULL, file_async_parse, fr);
    pthread_detach(async);
}
