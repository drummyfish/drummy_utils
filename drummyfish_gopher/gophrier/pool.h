/*  Copyright 2010-2011 Guillaume Duhamel

    This file is part of Gophrier.

    Gophrier is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    Gophrier is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Gophrier; if not, write to the Free Software
    Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301  USA
*/

#ifndef GOPHRIER_POOL_H
#define GOPHRIER_POOL_H

#include "sock.h"

struct _SocketPool
{
    Socket ** sockets;
    int maxSocket;
    int running;
    int argc;
    char ** argv;
};

SocketPool * socket_pool_new(Socket * s, int argc, char ** argv);
void socket_pool_free(SocketPool * pool);
void socket_pool_add(SocketPool * pool, Socket * s);
void socket_pool_remove(SocketPool * pool, Socket * s);
void socket_pool_fill_fd_set(SocketPool * pool, fd_set * fs);
void socket_pool_run(SocketPool * pool);
int socket_pool_start(SocketPool * pool);
void socket_pool_stop(SocketPool * pool);
void socket_pool_reload(SocketPool * pool);

#endif
