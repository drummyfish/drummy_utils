/*  Copyright 2010-2011 Guillaume Duhamel

    This file is part of Gophrier.

    Gophrier is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    Gophrier is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Gophrier; if not, write to the Free Software
    Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301  USA
*/

#include "log.h"
#include "conf.h"

#include <stdio.h>
#include <time.h>
#include <stdlib.h>
#include <string.h>
#include <stdarg.h>

static int nolog = 0;
static FILE * logfile = NULL;

static int log_open();

void log_append(const char * format, ...)
{
    va_list l;
    time_t now;
    struct tm logtime;

    va_start(l, format);

    if (! log_open()) return;

    time(&now);
    localtime_r(&now, &logtime);

    fprintf(logfile, "%04d/%02d/%02d %02d:%02d:%02d - ", 
        1900 + logtime.tm_year,
        1 + logtime.tm_mon,
        logtime.tm_mday,
        logtime.tm_hour,
        logtime.tm_min,
        logtime.tm_sec);
    vfprintf(logfile, format, l);
    fprintf(logfile, "\n");
    fflush(logfile);

    va_end(l);
}

void log_reopen()
{
    if (logfile)
    {
        fclose(logfile);
        logfile = NULL;
    }
}

static int log_open()
{
    if (nolog) return 0;
    if (logfile) return 1;

    {
        char * logfilename;
        char * logdir;
        
        logdir = conf_get_log_dir();
        logfilename = (char *) malloc(strlen(logdir) + 1 + strlen("gophrier.log") + 1);
        sprintf(logfilename, "%s/%s", logdir, "gophrier.log");
        logfile = fopen(logfilename, "a");
        free(logfilename);
        if (! logfile)
        {
            nolog = 1;
            return 0;
        }
    }

    return 1;
}
