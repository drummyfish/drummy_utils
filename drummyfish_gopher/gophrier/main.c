/*  Copyright 2010-2011 Guillaume Duhamel

    This file is part of Gophrier.

    Gophrier is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    Gophrier is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Gophrier; if not, write to the Free Software
    Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301  USA
*/

#include "pool.h"
#include "server.h"
#include "conf.h"
#include "cli.h"
#include "hand.h"
#include "log.h"

#include <unistd.h>

int main(int argc, char ** argv)
{
    Socket * sock;
    Socket * hand;
    SocketPool * pool;

    /* there might be fatal errors here, so we don't
     * want to be a daemon yet so we can tell the user
     * about it */
    cli_parse(argc, argv);

    sock = server_socket_new();
    pool = socket_pool_new(sock, argc, argv);

    hand = hand_new();
    socket_pool_add(pool, hand);

    /* it should ok to turn to a daemon now...
     * Am I aevil? */
    if (conf_is_daemon())
    {
        FILE * pidfile;
        /* Yes I am! */
        daemon(0, 0);

        pidfile = fopen(conf_get_pid_path(), "w");
        if (pidfile)
        {
            fprintf(pidfile, "%d\n", getpid());
            fclose(pidfile);
        }
        else
            log_append("failed to open pid file: %s", conf_get_pid_path());
    }

    return socket_pool_start(pool);
}
