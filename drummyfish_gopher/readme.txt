My gopherhole, update.sh should be run automatically e.g. once a day to auto
mirror stuff. Tested server with this is gophier (included in a subdir with some
custom changes), use as:

gophrier -b 1 -r /pathtothisdir --host "hostnameoftheserver"

The --host option is IMPORTANT, don't forget it!
