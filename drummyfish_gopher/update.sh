#!/bin/sh

# update site mirror:

pandoc -s -r html http://www.tastyfish.cz -t plain | iconv -f utf-8 -t ascii//TRANSLIT > tmp.txt

LINE=`grep -n -m 1 "About Me" tmp.txt | grep -Po "^[0-9]+"`

cat tmp.txt | tail -n +$LINE > tastyfish.txt

rm tmp.txt

# update files:

cd files

[ ! -d "./SAF" ] && git clone https://gitlab.com/drummyfish/SAF.git
cd SAF
git pull
cd ..

[ ! -d "./Anarch" ] && git clone https://gitlab.com/drummyfish/Anarch.git
cd Anarch
git pull
cd ..

[ ! -d "./my_text_data" ] && git clone https://gitlab.com/drummyfish/my_text_data.git
cd my_text_data
git pull
cd ..

[ ! -d "./my_writings" ] && git clone https://gitlab.com/drummyfish/my_writings.git
cd my_writings
git pull
cd ..

[ ! -d "./small3dlib" ] && git clone https://gitlab.com/drummyfish/small3dlib.git
cd small3dlib
git pull
cd ..

[ ! -d "./smallchesslib" ] && git clone https://gitlab.com/drummyfish/smallchesslib.git
cd smallchesslib
git pull
cd ..

[ ! -d "./raycastlib" ] && git clone https://gitlab.com/drummyfish/raycastlib.git
cd SAF
git pull
cd ..

cd ..

# update LRS wiki:

[ ! -d "./less_retarded_wiki" ] && git clone https://gitlab.com/drummyfish/less_retarded_wiki.git 

cd less_retarded_wiki
git pull
./make_txt.sh
cd ..
rm -rf lrs
mkdir lrs
cd lrs
cp ../less_retarded_wiki/txt/*.txt .

echo "== LESS RETARDED WIKI ==" > gophermap
echo "by drummyfish, realeased under CC0 1.0, public domain" >> gophermap
echo "This is an auto-created gopher txt version of my LRS Wiki." >> gophermap
echo "Unfortunatelly hyperlink are lost in txt." >> gophermap
echo "0main page\tmain.txt" >> gophermap
echo "all articles:" >> gophermap

for f in *.txt; do
  fname=$(echo "$f" | sed "s/\.txt//g")

  echo "0${fname}\t/$f" >> gophermap
done

cd ..
