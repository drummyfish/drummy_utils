# by drummyfish, CC0 1.0 (public domain)

from PIL import Image
import sys

def rgbTo565(r, g, b):
  r = r >> 3
  g = g >> 2
  b = b >> 3
  
  return (r << 11) | (g << 5) | b 

def rgbFrom565(rgb565):
  return (
    ((rgb565 & int('1111100000000000',2)) >> 11) << 3,
    ((rgb565 & int('0000011111100000',2)) >> 5) << 2,
    (rgb565 & int('0000000000011111',2)) << 3)

result = []

image = Image.open(sys.argv[1]).convert("RGB")
pixels = image.load()

image2 = Image.new("RGB",image.size,"black")
pixels2 = image2.load()

for y in range(image.size[1]):
  for x in range(image.size[0]):
    p = pixels[x,y];

    p565 = rgbTo565(p[0],p[1],p[2])

    adjustDir = 1 if x % 2 == y % 2 else -1

    while True:
      isUnique = True

      for p2 in result:
        if p2 == p565:
          isUnique = False
          break

      if isUnique:
        break
 
      print("pixel at " + str(x) + ";" + str(y) + " not unique, adjusting")

      p565 += adjustDir

    result.append(p565)
    pixels2[x,y] = rgbFrom565(p565)

print(result)
image2.save("palette565.png")
