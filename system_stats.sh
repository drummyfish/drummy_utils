#!/bin/sh
# Shows some overall system info, e.g. for generating report file for a custom
# server. Not necessarily super portable.

echo "=== SYSTEM STATS ==="

str=$(date +"%d.%m.%Y %T")
echo "generated on $str"
echo "---system---"
uname -a
str=$(ps -e | wc -l)
echo "processes: $str"
echo "---CPU---"
str=$(cat /proc/loadavg | awk '{ print $3 }')
cat /proc/cpuinfo | grep 'model name\|cpu MHz\|cpu cores\|cache size' | sort | uniq
echo "average CPU load over 15 minutes (no. of processes waiting for CPU, 1 ~= 100%) : $str"
echo "---RAM---"
cat /proc/meminfo | grep 'MemTotal\|MemAvailable'
echo "---disk---"
df -h
echo "--network--"
echo "IP:"
netstat -s | grep 'total packets received\|requests sent out'
echo "TCP:"
netstat -s | grep "segments\|active connection openings"
echo "UDP:"
netstat -s | grep "packets received\|packets sent"
