/** Simple utility for printing trees horizontally.

by drummyfish, 2022, released under CC0 1.0, public domain
(https://creativecommons.org/publicdomain/zero/1.0/) */

#include <stdio.h>

#define MAX_STRING_SIZE 16
#define MAX_LEVELS 128
#define MAX_INPUT_LENGTH 65536

typedef struct
{
  char name[MAX_STRING_SIZE];
  unsigned int childCount;
  const char *str;
} Node;

char inputStr[MAX_INPUT_LENGTH + 1];
unsigned char fixedNameLen = 5;
unsigned int lineLen = 5;
char graphicChars[] = "~|',";

void padName(char *name, unsigned int pad)
{
  int padding = 0;

  for (unsigned int i = 0; i < pad; ++i)
  {
    if (*name == 0)
      padding = 1;

    if (padding)
      *name = ' ';

    name++;
  }

  *name = 0;
}

void sanitizeName(char *name)
{
  while (*name != 0)
  {
    if (*name == '\n' || *name == '\t')
    {
      char *s = name;

      while (*s != 0)
      {
        *s = *(s + 1);
        s++;
      }
    }
    else
      name++;
  }
}

int nodeFromStr(const char *str, Node *result)
{
  unsigned int namePos = 0;
  result->name[0] = 0;
  result->childCount = 0;
  result->str = str;

  while (*str != ')' && *str != ',' && *str != ';' && *str != 0)
  {
    if (*str != '(')
    {
      if (namePos < MAX_STRING_SIZE - 1)
      {
        result->name[namePos] = *str;
        namePos++;
        result->name[namePos] = 0;
      }
    }
    else
    {
      result->childCount = 1;
      int parenthisCount = 0;

      while (1)
      {
        if (*str == 0)
        {
          fputs("ERROR: unexpected end of input in node parsing",stderr);
          return 0;
        }

        str++;

        if (*str == '(')
          parenthisCount++;
        else if (*str == ')')
        {
          if (parenthisCount == 0)
            break;

          parenthisCount--;
        }
        else if (*str == ',' && parenthisCount == 0)
          result->childCount++;
      }
    }

    str++;
  }

  sanitizeName(result->name);
  padName(result->name,fixedNameLen);

  return 1;
}

const char *getNodeChild(const Node *node, unsigned int index)
{
  const char *s = node->str;

  while (*s != '(' && *s != 0)
    s++;

  if (*s == 0)
  {
    fputs("ERROR: expected '(' but didn't find it",stderr);
    return 0;
  }

  s++;
    
  int parenthisCount = 0;

  while (index != 0)
  {
    if (*s == 0)
    {
      fputs("ERROR: unexpected end of input in child parsing",stderr);
      return 0;
    }
    if (*s == '(')
      parenthisCount++;
    else if (*s == ')')
    {
      if (parenthisCount == 0)
        break;

      parenthisCount--;
    }
    else if (*s == ',' && parenthisCount == 0)
      index--;

    s++;
  } 

  return s;
}

unsigned char levels[MAX_LEVELS];

int _treePrintHorizontal(const char *str,unsigned int level,char type)
{
  Node n;

  if (!nodeFromStr(str,&n))
    return 0;

  unsigned int childIndex = 0;

  levels[level] = type % 2 == 0;

  unsigned char up = 1;

  while (1)
  {
    if (childIndex == n.childCount / 2)
    {
      for (unsigned int i = 0; i < level; ++i)
      {
        putchar(levels[i] ? graphicChars[1] : ' ');

        for (unsigned int j = 0; j < lineLen + fixedNameLen; ++j)
          putchar(' ');
      }

      switch (type)
      {
        case 0: putchar(graphicChars[2]); break;
        case 1: putchar(graphicChars[3]); break;
        case 2: putchar(graphicChars[1]); break;
        default: break;
      }

      if (level == 0)
        putchar(graphicChars[0]);

      for (unsigned int j = 0; j < lineLen / 2; ++j)
        putchar(graphicChars[0]);

      printf("%s",n.name);

      if (n.childCount != 0)
      {
        for (unsigned int j = 0; j < lineLen - lineLen / 2; ++j)
          putchar(graphicChars[0]);

        putchar(n.childCount == 1 ? graphicChars[3] : graphicChars[1]);
      }

      putchar('\n');

      if (type <= 1)
        levels[level] = !levels[level];

      up = 0;
    }

    if (childIndex < n.childCount)
    {
      const char *c = getNodeChild(&n,childIndex);

      if (c == 0)
        return 0;

      if (!_treePrintHorizontal(c,level + 1,
        (childIndex == 0 || childIndex == n.childCount - 1) ? up : 2))
        return 0;
    }
    else
      break;

    childIndex++;
  }

  return 1;
}

int treePrintHorizontal(const char *str)
{
  return _treePrintHorizontal(str,0,3);
}

unsigned char charToDigit(char c)
{
  if (c >= '0' && c <= '9')
    return c - '0';
 
  if (c >= 'a' && c <= 'z')
    return c - 'a' + 10;

  if (c >= 'A' && c <= 'Z')
    return c - 'A' + 10;

  return 0;
}

void printHelp(void)
{
  puts("htree: print tree horizontally");
  puts("by drummyfish, released under CC0");
  puts("input is read from stdin, tree is in Newick-like format, e.g.: (a,(b,c)NAME,d)");
  puts("options:");
  puts("  -h      print help and exit");
  puts("  -lX     set line length to X (X is in base 36)");
  puts("  -nX     set name length to X (X is in base 36)");
  puts("  -cABCD  set graphical characters to A, B, C and D");
}

int main(int argc, char **argv)
{
  for (int i = 1; i < argc; ++i)
  {
    if (argv[i][0] == '-')
    {
      switch (argv[i][1])
      {
        case 'c':
        {
          for (int j = 0; j < 4; ++j)
            if (argv[i][j + 2] == 0)
              break;
            else
              graphicChars[j] = argv[i][j + 2];

          break;
        }

        case 'l': if (argv[i][2] != 0) lineLen = charToDigit(argv[i][2]);
          break;

        case 'n': if (argv[i][2] != 0) fixedNameLen = charToDigit(argv[i][2]);
          break;

        case 'h': 
          printHelp();
          return 0;
          break;

        default: break;
      }
    }
  }

  unsigned int pos = 0;

  while (pos < MAX_INPUT_LENGTH)
  {
    int c = getchar();

    if (c == EOF)
      break;

    inputStr[pos] = c;
    pos++;
  }

  inputStr[pos] = 0;

  return treePrintHorizontal(inputStr) == 0;
}
