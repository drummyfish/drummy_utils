/**
  Test for drummylib.

  by Drummyfish, released under CC0 1.0, public domain
*/

#define DFL_STDLIB        // the test should work even without this define
#define DFL_LOG(s) printf("  DFL: %s\n",s)

#include "drummylib.h"
#include <stdio.h>

void printTest(const char *text)
{
  printf("~testing %s",text);
}

int assert(int cond, const char *text)
{
  printTest(text);

  if (cond)
  {
    puts(": OK");
    return 1;
  }
  else
  {
    puts(": ERROR");
    return 0;
  }
}

int main(int argc, const char** argv)
{
  puts("Testing drummylib:\n");

  #define ass(c,t) printf("(line %d) ",__LINE__); if (!assert(c,t)) return 1

  ass(DFL_B(10101) == 21,"binary macro");
  ass(DFL_B(111111111111) == 4095,"binary macro");
  ass(DFL_B(10010110) == DFL_10010110,"binary macro");

  ass(DFL_factorial32(7) == 5040,"factorial");
  ass(DFL_factorial32(9) == DFL_factorial64(9),"factorial");

  ass(DFL_stringLength("test") == 4,"string length");

  ass(!DFL_stringEquals("abc","abcd"),"string equals");
  ass(!DFL_stringEquals("abcde","abcd"),"string equals");

  ass((DFL_SIGN(-10 + 20) == 1) && (DFL_SIGN(5 - 5) == 0) && (DFL_SIGN(-100) == -1),"sign macro"); 

  {
    ass(DFL_serializeI32(DFL_streamToNull,-123,10,0) == 4,"serialization count");
    ass(DFL_serializeI32(DFL_streamToNull,11,1,0) == 11,"serialization count");
    ass(DFL_serializeU32(DFL_streamToNull,2000,10,0) == 4,"serialization count");
    ass(DFL_serializeU64(DFL_streamToNull,DFL_U64_MAX,10,0) == 20,"serialization count");
    ass(DFL_serializeF(DFL_streamToNull,12.750,2,0) == 7,"serialization count");
  }

  {
    char testText[64] = "Test text\nnew line.";
    char testText2[64] = "different string";
    char testText3[8];
/*
    ass(DFL_fileWrite((uint8_t *) testText,DFL_stringLength(testText),"dfl_test.txt"),"file save");

    #ifdef DFL_STDLIB
      ass(DFL_fileReadString(testText2,64,"dfl_test.txt"),"file read");
      ass(DFL_stringEquals(testText,testText2),"string comparison");
      ass(DFL_fileReadString(testText3,8,"dfl_test.txt"),"file read");
      ass(DFL_stringLength(testText3) == 7,"string length");
    #endif
*/
  }

  {
    char string[64];
    
    #define testIntSerial(w,n,b,z,s) \
      DFL_streamEndToStr(DFL_stream ## w ## ToStr (DFL_stringInit(string,64),n,b,z));\
      ass(DFL_stringEquals(string,s),"integer serialization");

      testIntSerial(I32,0,10,0,"0");
      testIntSerial(U32,0,10,1,"0");
      testIntSerial(U64,0,10,0,"0");
      testIntSerial(I64,0,10,2,"00");

      testIntSerial(I32,-123456789,10,5,"-123456789");
      testIntSerial(U32,4294967295,10,0,"4294967295");

      testIntSerial(U64,18446744073709551615,10,1,"18446744073709551615");
      testIntSerial(I64,500000000000,10,0,"500000000000");

    #undef testIntSerial
  }

  {
    char testText[] = "Testing string 123.";
    ass(DFL_stringEquals(DFL_stringReverse(testText),".321 gnirts gnitseT"),"string reverse");

    printTest("prints");

    putchar('\n');

    char testText2[] = "line1\nline2\n";
    DFL_printS(testText2);
    DFL_printC('a');
    DFL_printC('b');
    DFL_printC('c');
    DFL_printC('\n');
  }

  {
    char program[] = "++++++++++  [> + + + + + + + COMMENT [ >+<- ] <-]>> ";

    char tape[16];

    for (uint16_t i = 0; i < 16; ++i)
      
tape[i] = 0;

    DFL_brainfuckRun(program,tape,16,0,0);

    ass(tape[2] == 70,"brainfuck");    
  }

  {
    uint8_t ok = 1;

    for (uint16_t i = 1; i <= DFL_ROMAN_MAX; ++i)
    {
      char buf[] = "                   ";
      DFL_streamRomanToStr(buf,i);
      uint16_t r;
      DFL_streamRomanFromStr(buf,&r);

      if (i != r)
      {
        ok = 0;
        break;
      }
    }
   
    ass(ok,"roman numerals"); 
  }

  {
    uint8_t m1[1024];
    uint8_t m2[1024];

    ass(DFL_memoryCompare(DFL_memoryCopy(DFL_memorySet(m1,1024,42),1024,m2),m1,1024),"memory functions");
  }

  {
    ass(DFL_floatsEqual(DFL_MOD_F(5.2,2),1.2,0.01),"modf macro");
    ass(DFL_floatsEqual(DFL_MOD_F(-5.2,6),-5.2,0.01),"modf macro");
    ass(DFL_floatsEqual(DFL_COS_F(0.0),1.0,0.01),"cos macro");
    ass(DFL_floatsEqual(DFL_COS_F(DFL_PI_F / 2 - DFL_PI_F * 4),0.0,0.01),"cos macro");
    ass(DFL_WRAP(-9,4) == 3,"wrap macro");

    int8_t ok = 1;

    for (float f = -12345.100; f < 456789.1000; f += 15327.77)
    {
      char buf[16];
      float g = 0.0;
      DFL_streamEndToStr(DFL_streamFToStr(buf,f,10,5));
      DFL_streamFFromStr(buf,10,&g);

      if (!DFL_floatsEqual(f,g,0.5))
      {
        ok = 0;
        break;
      }
    }

    ass(ok,"float (de)serialization");

    float g;
    DFL_streamFFromStr(".111",2,&g);
    ass(g == 0.875,"binary float");

    DFL_streamFFromStr(".125E3",10,&g);
    ass(g == 125,"exponential float");
  }

  {
    uint8_t ac = 10;
    const char *av[] =
    {
      "./program",
      "a",
      "b",
      "-a5",
      "-b",
      "-ifile.txt",
      "-ifile2.txt",
      "",
      "126",
      "11.56"
    };

    ass(DFL_CLIArgPresent('b',ac,av),"CLI arguments");
    ass(!DFL_CLIArgPresent('c',ac,av),"CLI arguments");
    ass(DFL_CLIArgGet0to9('a',255,ac,av) == 5,"CLI arguments 0-9");
    ass(DFL_CLIArgGet0to9(0,255,ac,av) == 255,"CLI arguments 0-9");
    ass(DFL_stringEquals(DFL_CLIArgGetStr('i',"none",ac,av),"file.txt" ),"CLI arguments str");
    ass(DFL_stringEquals(DFL_CLIArgGetStr(0,"a",ac,av),"a" ),"CLI arguments str");
    ass(DFL_CLIArgGetI32(3,0,ac,av) == 126,"CLI arguments I32");
    ass(DFL_CLIArgGetI32(1,-200,ac,av) == -200,"CLI arguments I32 fail");
  }

  {
    #define l 26
    uint8_t bitArray[DFL_BIT_ARRAY_SIZE(l)];

    for (uint8_t i = 0; i < l; ++i)
      DFL_bitArraySet(bitArray,i,i != 6 ? (i % 3) : 1);

    char buffer[64];

    DFL_streamEndToStr(DFL_streamBitArrayToStr(DFL_stringInit(buffer,64),bitArray,l));

    ass(DFL_stringEquals(buffer,"01101111101101101101101101"),"bit array serialization");

    uint8_t ok = 1;

    for (uint8_t i = 0; i < l; ++i)
      if (DFL_bitArrayGet(bitArray,i) != ((i != 6 ? (i % 3) : 1) > 0))
      {
        ok = 0;
        break;
      }

    ass(ok,"bit array get");

    #undef l
  }

  puts("--------\neverything OK\n");

//DFL_streamFFromStr("  123.25  ",10,&fff);

//DFL_streamEndToStr(DFL_streamFToStr(aaa,123.321,10,10));

//DFL_printF(10.0);
 
/* 
float g;
  
DFL_streamFFromStr("-123.75000",10,&g);

printf("%f\n",g);
*/

/*
DFL_ChessState chess = DFL_CHESS_START_STATE;

DFL_printLn();
DFL_serializeChessState(DFL_streamToOut,chess,0);
DFL_printLn();

DFL_ChessMoveSet moves;

DFL_chessGetPieceMoves(chess,17,moves);

uint8_t p = 0;

while (moves[p] != 255)
{
  printf("%d\n",moves[p]);
  p++;
}
*/

/*
uint8_t aaa[30];
char fuck[] = "fuuuck"; 

for (uint8_t i = 0; i < 30; ++i)
  aaa[i] = i * 2000;

DFL_serializeMemoryDump(DFL_streamToOut,(uint8_t *) fuck, 1,16,6,10,0,10);
*/

//DFL_streamU32ToOut(1441812000,10,5);

//DFL_serializeU64(DFL_streamToOut,DFL_U64_MAX / 100000,10,20);
//DFL_serializeU64(DFL_streamToOut,18446744073709551615,10,4);
//DFL_serializeU32(DFL_streamToOut,DFL_U64_MAX,10,0);

//printf("~%d~\n",DFL_SYSTEM_ADDRESS_WIDTH);


  return 0;
}
