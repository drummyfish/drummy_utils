#!/bin/bash

# Tries to configure mouse (Bloody V7MA).
# Can be set as a after-startup script.

echo "Fixing mouse!"

SEARCH="COMPANY USB Device"

ids=$(xinput --list | grep "pointer" | awk -v search="$SEARCH" \
    '$0 ~ search {match($0, /id=[0-9]+/);\
                  if (RSTART) \
                    print substr($0, RSTART+3, RLENGTH-3)\
                 }'\
     )

for i in $ids
do
    echo "Fixing device with ID = $i..."
    xinput set-prop $i 'Device Accel Profile' -1
    xinput set-prop $i 'Device Accel Constant Deceleration' 1.0
    xinput set-prop $i 'Device Accel Velocity Scaling' 1.0
done
