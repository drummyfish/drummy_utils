from PIL import Image
from PIL import ImageFilter
from PIL import ImageEnhance
import sys

image = Image.open(sys.argv[1]).convert("RGB")
image2 = Image.new("RGB",(image.size[0] * 3,image.size[1] * 3),color="white")

pixels = image.load()
pixels2 = image2.load()

for y in range(image.size[1]):
  for x in range(image.size[0]):
    p = pixels[x,y]
    p2 = (int(p[0] * 0.5),int(p[1] * 0.5),int(p[2] * 0.5))
    #p3 = (int(p[0] * 0.4),int(p[1] * 0.4),int(p[2] * 0.4))

    sl = 0.9 # scanline

    xx = x * 3
    yy = y * 3

    c = (p[0],p2[1],p2[2])
    pixels2[xx,yy] = c
    pixels2[xx,yy + 1] = c
    c = (int(c[0] * sl),int(c[1] * sl),int(c[2] * sl))
    pixels2[xx,yy + 2] = c

    xx += 1

    c = (p2[0],p[1],p2[2])
    pixels2[xx,yy] = c
    pixels2[xx,yy + 1] = c
    c = (int(c[0] * sl),int(c[1] * sl),int(c[2] * sl))
    pixels2[xx,yy + 2] = c

    xx += 1

    c = (p2[0],p2[1],p[2])
    pixels2[xx,yy] = c
    pixels2[xx,yy + 1] = c
    c = (int(c[0] * sl),int(c[1] * sl),int(c[2] * sl))
    pixels2[xx,yy + 2] = c

# bloom:

for y in range(image.size[1]):
  for x in range(image.size[0]):
    p = pixels[x,y]

    if p[0] + p[1] + p[2] < 450:
      pixels[x,y] = 0

image = image.resize((image.size[0] * 3,image.size[1] * 3))
image = image.filter(ImageFilter.GaussianBlur(1))

pixels = image.load()

for y in range(image.size[1]):
  for x in range(image.size[0]):
    p = pixels[x,y]
    p2 = pixels2[x,y]

    if p[0] + p[1] + p[2] > p2[0] + p2[1] + p2[2]:
      p2 = ((p[0] + p2[0]) / 2,(p[1] + p2[1]) / 2,(p[2] + p2[2]) / 2)
      pixels2[x,y] = p2

image2 = ImageEnhance.Brightness(image2).enhance(1.25)
image2 = ImageEnhance.Contrast(image2).enhance(1.05)

image2.save("i.png")
