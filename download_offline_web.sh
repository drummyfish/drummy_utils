#!/bin/bash
#
# This is a script that downloads a selected subset of the Web to browse when
# offline.
#
# by drummyfish, released under CC0 1.0 (public domain)

rm -rf offline
mkdir offline
clear

echo "
  https://infogalactic.com/info/Main_Page
  https://infogalactic.com/info/Infogalactic
  https://infogalactic.com/info/Race_(human_categorization)
  https://infogalactic.com/info/Wikipedia
  https://infogalactic.com/info/Jews
  https://infogalactic.com/info/4chan
  https://infogalactic.com/info/8chan

  https://en.metapedia.org/wiki/Anders_Behring_Breivik
  https://en.metapedia.org/wiki/Adolf_Hitler
  https://en.metapedia.org/wiki/Main_Page
  https://en.metapedia.org/wiki/Race
  https://en.metapedia.org/wiki/Wikipedia
  https://en.metapedia.org/wiki/Out_of_Africa
  https://en.metapedia.org/wiki/Multiregional_origin_of_modern_humans
  https://en.metapedia.org/wiki/Jew
  https://en.metapedia.org/wiki/Holocaust

  https://en.wikipedia.org/wiki/Main_Page
  https://en.wikipedia.org/wiki/User:Drummyfish
  https://en.wikipedia.org/wiki/User:Drummyfish/Books/Tastyfish%27s_Book
  https://en.wikipedia.org/wiki/Time_Cube
  https://en.wikipedia.org/wiki/C_(programming_language)
  https://en.wikipedia.org/wiki/C%2B%2B
  https://en.wikipedia.org/wiki/Java_(programming_language)
  https://en.wikipedia.org/wiki/Javascript
  https://en.wikipedia.org/wiki/Lisp
  https://en.wikipedia.org/wiki/Haskell_(programming_language)
  https://en.wikipedia.org/wiki/Mathematics
  https://en.wikipedia.org/wiki/Physics
  https://en.wikipedia.org/wiki/Science
  https://en.wikipedia.org/wiki/Biology
  https://en.wikipedia.org/wiki/Chemistry
  https://en.wikipedia.org/wiki/History
  https://en.wikipedia.org/wiki/Philosophy
  https://en.wikipedia.org/wiki/Linguistics
  https://en.wikipedia.org/wiki/Psychology
  https://en.wikipedia.org/wiki/Geography
  https://en.wikipedia.org/wiki/Computer_science
  https://en.wikipedia.org/wiki/Quantum_mechanics
  https://en.wikipedia.org/wiki/Computer_graphics
  https://en.wikipedia.org/wiki/Economy
  https://en.wikipedia.org/wiki/Capitalism
  https://en.wikipedia.org/wiki/Communism
  https://en.wikipedia.org/wiki/Anarchism
  https://en.wikipedia.org/wiki/Anarcho-pacifism
  https://en.wikipedia.org/wiki/Mathematical_proof
  https://en.wikipedia.org/wiki/Wikipedia
  https://en.wikipedia.org/wiki/Quaternion
  https://en.wikipedia.org/wiki/Bicameralism_(psychology)
  https://en.wikipedia.org/wiki/Schizophrenia
  https://en.wikipedia.org/wiki/Bipolar_disorder
  https://en.wikipedia.org/wiki/Borderline_disorder
  https://en.wikipedia.org/wiki/Monad_(category_theory)
  https://en.wikipedia.org/wiki/Monad_(functional_programming)
  https://en.wikipedia.org/wiki/Category_theory
  https://en.wikipedia.org/wiki/3D_rotation_group
  https://en.wikipedia.org/wiki/Timeline_of_ancient_history
  https://en.wikipedia.org/wiki/Timeline_of_the_far_future
  https://en.wikipedia.org/wiki/4chan
  https://en.wikipedia.org/wiki/Free_software
  https://en.wikipedia.org/wiki/Linux
  https://en.wikipedia.org/wiki/Linux_kernel
  https://en.wikipedia.org/wiki/OpenBSD
  https://en.wikipedia.org/wiki/History_of_computing_hardware
  https://en.wikipedia.org/wiki/Artificial_neural_network
  https://en.wikipedia.org/wiki/Architecture_of_Windows_NT
  https://en.wikipedia.org/wiki/Convolutional_neural_network
  https://en.wikipedia.org/wiki/Deep_learning
  https://en.wikipedia.org/wiki/Comparison_of_file_systems
  https://en.wikipedia.org/wiki/Doom_(1993_video_game)
  https://en.wikipedia.org/wiki/Duke_Nukem_3D
  https://en.wikipedia.org/wiki/Anti-pattern
  https://en.wikipedia.org/wiki/Software_design_pattern
  https://en.wikipedia.org/wiki/Build_(game_engine)
  https://en.wikipedia.org/wiki/Doom_engine
  https://en.wikipedia.org/wiki/ARM_architecture
  https://en.wikipedia.org/wiki/Comparison_of_operating_system_kernels
  https://en.wikipedia.org/wiki/Esperanto  
  https://en.wikipedia.org/wiki/Interslavic
  https://en.wikipedia.org/wiki/Adolf_Hitler
  https://en.wikipedia.org/wiki/Tunguska_event
  https://en.wikipedia.org/wiki/Wikipedia:Unusual_articles
  https://en.wikipedia.org/wiki/Wikipedia:Silly_Things
  https://en.wikipedia.org/wiki/Wikipedia:Wikipedia_records
  https://en.wikipedia.org/wiki/Wikipedia:Don%27t_delete_the_main_page
  https://en.wikipedia.org/wiki/Toynbee_tiles
  https://en.wikipedia.org/wiki/Wikipedia:Wikipedia_records
  https://en.wikipedia.org/wiki/Year_Without_a_Summer
  https://en.wikipedia.org/wiki/List_of_eponymous_laws
  https://en.wikipedia.org/wiki/Nobel_Prize_controversies
  https://en.wikipedia.org/wiki/Wine-dark_sea_(Homer)
  https://en.wikipedia.org/wiki/Cicada_3301
  https://en.wikipedia.org/wiki/Julian_Assange
  https://en.wikipedia.org/wiki/Che_Guevara
  https://en.wikipedia.org/wiki/Anders_Behring_Breivik
  https://en.wikipedia.org/wiki/NetBSD
  https://en.wikipedia.org/wiki/Supercomputer
  https://en.wikipedia.org/wiki/List_of_cognitive_biases
  https://en.wikipedia.org/wiki/Cognitive_bias_in_animals
  https://en.wikipedia.org/wiki/List_of_conspiracy_theories
  https://en.wikipedia.org/wiki/Allosaurus
  https://en.wikipedia.org/wiki/Earth
  https://en.wikipedia.org/wiki/Moon
  https://en.wikipedia.org/wiki/Mars
  https://en.wikipedia.org/wiki/Glossary_of_chess
  https://en.wikipedia.org/wiki/Chess
  https://en.wikipedia.org/wiki/Computer_chess
  https://en.wikipedia.org/wiki/Japan
  https://en.wikipedia.org/wiki/Asia
  https://en.wikipedia.org/wiki/China
  https://en.wikipedia.org/wiki/Universe
  https://en.wikipedia.org/wiki/Milky_way
  https://en.wikipedia.org/wiki/Andromeda_Galaxy
  https://en.wikipedia.org/wiki/Venus
  https://en.wikipedia.org/wiki/Cristiano_Ronaldo
  https://en.wikipedia.org/wiki/Noam_Chomsky
  https://en.wikipedia.org/wiki/Richard_Stallman
  https://en.wikipedia.org/wiki/Kurt_G%C3%B6del
  https://en.wikipedia.org/wiki/Isaac_Newton
  https://en.wikipedia.org/wiki/Steve_jobs  
  https://en.wikipedia.org/wiki/Bill_gates
  https://en.wikipedia.org/wiki/Einstein
  https://en.wikipedia.org/wiki/Leonhard_Euler
  https://en.wikipedia.org/wiki/Nikola_Tesla
  https://en.wikipedia.org/wiki/Alan_Turing
  https://en.wikipedia.org/wiki/Eric_S._Raymond
  https://en.wikipedia.org/wiki/Donald_Knuth
  https://en.wikipedia.org/wiki/Unix
  https://en.wikipedia.org/wiki/Dennis_Ritchie  
  https://en.wikipedia.org/wiki/Linus_Torvalds
  https://en.wikipedia.org/wiki/Demoscene
  https://en.wikipedia.org/wiki/Antarctica
  https://en.wikipedia.org/wiki/Internet
  https://en.wikipedia.org/wiki/Prehistory
  https://en.wikipedia.org/wiki/Alien_hand_syndrome
  https://en.wikipedia.org/wiki/Gopher
  https://en.wikipedia.org/wiki/Jesus
  https://en.wikipedia.org/wiki/World_War_II
  https://en.wikipedia.org/wiki/World_War_I
  https://en.wikipedia.org/wiki/Vietnam_War
  https://en.wikipedia.org/wiki/Coronavirus_disease_2019
  https://en.wikipedia.org/wiki/Scientology
  https://en.wikipedia.org/wiki/Law
  https://en.wikipedia.org/wiki/Human
  https://en.wikipedia.org/wiki/Taxonomy_(biology)
  https://en.wikipedia.org/wiki/United_States
  https://en.wikipedia.org/wiki/Moravia
  https://en.wikipedia.org/wiki/Dinosaur
  https://en.wikipedia.org/wiki/Life
  https://en.wikipedia.org/wiki/List_of_numbers
  https://en.wikipedia.org/wiki/The_Hum
  https://en.wikipedia.org/wiki/Far_future_in_fiction
  https://en.wikipedia.org/wiki/POSIX
  https://en.wikipedia.org/wiki/Extreme_points_of_Earth
  https://en.wikipedia.org/wiki/List_of_unsolved_problems_in_mathematics
  https://en.wikipedia.org/wiki/List_of_world_records_in_athletics
  https://en.wikipedia.org/wiki/Qubit
  https://en.wikipedia.org/wiki/Quantum_computing
  https://en.wikipedia.org/wiki/Terry_A._Davis
  https://en.wikipedia.org/wiki/Intelligence_quotient
  https://en.wikipedia.org/wiki/Grand_Theft_Auto
  https://en.wikipedia.org/wiki/The_Elder_Scrolls
  https://en.wikipedia.org/wiki/Japanese_language
  https://en.wikipedia.org/wiki/Chinese_language
  https://en.wikipedia.org/wiki/Recent_African_origin_of_modern_humans
  https://en.wikipedia.org/wiki/Human_evolution
  https://en.wikipedia.org/wiki/Forth_(programming_language)
  https://en.wikipedia.org/wiki/Voyager_program
  https://en.wikipedia.org/wiki/ThinkPad
  https://en.wikipedia.org/wiki/Gigantopithecus
  https://en.wikipedia.org/wiki/Systemd
  https://en.wikipedia.org/wiki/Timeline_of_historic_inventions
  https://en.wikipedia.org/wiki/Theory_of_relativity
  https://en.wikipedia.org/wiki/General_relativity
  https://en.wikipedia.org/wiki/Modern_flat_Earth_societies
  https://en.wikipedia.org/wiki/List_of_micronations
  https://en.wikipedia.org/wiki/Pok%C3%A9mon
  https://en.wikipedia.org/wiki/1990s
  https://en.wikipedia.org/wiki/List_of_Latin_phrases_(full)
  https://en.wikipedia.org/wiki/Metaphysics
  https://en.wikipedia.org/wiki/William_Shakespeare
  https://en.wikipedia.org/wiki/Mahatma_Gandhi
  https://en.wikipedia.org/wiki/Lojban
  https://en.wikipedia.org/wiki/FreeDOS
  https://en.wikipedia.org/wiki/Anders_Behring_Breivik
  https://en.wikipedia.org/wiki/Color_theory
  https://en.wikipedia.org/wiki/Blockchain
  https://en.wikipedia.org/wiki/Kalman_filter
  https://en.wikipedia.org/wiki/Power_outage
  https://en.wikipedia.org/wiki/Axiom_of_choice
  https://en.wikipedia.org/wiki/Fuzzy_logic
  https://en.wikipedia.org/wiki/Demon_core
  https://en.wikipedia.org/wiki/Leo_Tolstoy
  https://en.wikipedia.org/wiki/Onion_routing
  https://en.wikipedia.org/wiki/Amundsen%E2%80%93Scott_South_Pole_Station
  https://en.wikipedia.org/wiki/Dodo
  https://en.wikipedia.org/wiki/Spanish_language
  https://en.wikipedia.org/wiki/Giza_pyramid_complex
  https://en.wikipedia.org/wiki/Unidentified_flying_object
  https://en.wikipedia.org/wiki/Extraterrestrial_life
  https://en.wikipedia.org/wiki/Rare_Earth_hypothesis
  https://en.wikipedia.org/wiki/Artificial_neural_network
  https://en.wikipedia.org/wiki/Comparison_of_Linux_distributions
  https://en.wikipedia.org/wiki/OpenBSD
  https://en.wikipedia.org/wiki/Comparison_of_operating_systems
  https://en.wikipedia.org/wiki/Unicode
  https://en.wikipedia.org/wiki/Supercomputer
  https://en.wikipedia.org/wiki/0
  https://en.wikipedia.org/wiki/Dream
  https://en.wikipedia.org/wiki/Karl_Marx
  https://en.wikipedia.org/wiki/List_of_algorithms
  https://en.wikipedia.org/wiki/List_of_film_and_television_accidents
  https://en.wikipedia.org/wiki/Asteroid_impact_avoidance
  https://en.wikipedia.org/wiki/List_of_chess_variants
  https://en.wikipedia.org/wiki/Shogi
  https://en.wikipedia.org/wiki/Go_(game)
  https://en.wikipedia.org/wiki/Xiangqi
  https://en.wikipedia.org/wiki/Historical_Jesus

  https://simple.wikipedia.org/wiki/Main_Page
  https://simple.wikipedia.org/wiki/Evolution
  https://simple.wikipedia.org/wiki/Human
  https://simple.wikipedia.org/wiki/United_States
  https://simple.wikipedia.org/wiki/Australia
  https://simple.wikipedia.org/wiki/History_of_the_world

  https://eo.wikipedia.org/wiki/Vikipedio:%C4%88efpa%C4%9Do

  https://nostalgia.wikipedia.org/wiki/HomePage
  https://nostalgia.wikipedia.org/wiki/AxiomOfChoice
  https://nostalgia.wikipedia.org/wiki/Race
  https://nostalgia.wikipedia.org/wiki/Internet_humor/Classified_ads
  https://nostalgia.wikipedia.org/wiki/Big_bang
  https://nostalgia.wikipedia.org/wiki/Earth
  https://nostalgia.wikipedia.org/wiki/History_of_computing
  https://nostalgia.wikipedia.org/wiki/Quantum_Mechanics
  https://nostalgia.wikipedia.org/wiki/Chess/Strategy_and_Tactics
  https://nostalgia.wikipedia.org/wiki/BadJokesAndOtherDeletedNonsense
  https://nostalgia.wikipedia.org/wiki/Video_game
  https://nostalgia.wikipedia.org/wiki/Roman_Empire
  https://nostalgia.wikipedia.org/wiki/Surreal_numbers
  https://nostalgia.wikipedia.org/wiki/Hyperreal_numbers 
  https://nostalgia.wikipedia.org/wiki/Metaphysics
  https://nostalgia.wikipedia.org/wiki/Philosophy
  https://nostalgia.wikipedia.org/wiki/Bible
  https://nostalgia.wikipedia.org/wiki/Ontology
  https://nostalgia.wikipedia.org/wiki/Aristotle
  https://nostalgia.wikipedia.org/wiki/Life
  https://nostalgia.wikipedia.org/wiki/Einstein
  https://nostalgia.wikipedia.org/wiki/Judaism
  https://nostalgia.wikipedia.org/wiki/Pi

  https://wiki.osdev.org/Expanded_Main_Page
  https://wiki.osdev.org/Introduction
  https://wiki.osdev.org/Bare_Bones
  https://wiki.osdev.org/Babystep1
  https://wiki.osdev.org/Babystep2
  https://wiki.osdev.org/Babystep3
  https://wiki.osdev.org/Babystep4
  https://wiki.osdev.org/Babystep5
  https://wiki.osdev.org/Boot_Sequence
  https://wiki.osdev.org/Real_Mode
  https://wiki.osdev.org/Protected_Mode
  https://wiki.osdev.org/ARM_Overview 
  https://wiki.osdev.org/PCI
  https://wiki.osdev.org/Creating_an_Operating_System
  https://wiki.osdev.org/Historical_Notes_on_CISC_and_RISC
  https://wiki.osdev.org/Required_Knowledge
  https://wiki.osdev.org/Beginner_Mistakes
  https://wiki.osdev.org/Getting_Started
  https://wiki.osdev.org/GCC_Cross-Compiler
  https://wiki.osdev.org/What_order_should_I_make_things_in
  https://wiki.osdev.org/Bootloader_FAQ
  https://wiki.osdev.org/Disk_Images
  https://wiki.osdev.org/Can_I_have_a_list_of_IO_Ports
  https://wiki.osdev.org/Everything_about_ports
  https://wiki.osdev.org/Languages
  https://wiki.osdev.org/What_filesystem_should_I_use
  https://wiki.osdev.org/USTAR
  https://wiki.osdev.org/FAT
  https://wiki.osdev.org/Object_Files
  https://wiki.osdev.org/Hard_Build_System
  https://wiki.osdev.org/Portability
  https://wiki.osdev.org/How_Do_I_Use_A_Debugger_With_My_OS
  https://wiki.osdev.org/Universal_Serial_Bus
  https://wiki.osdev.org/PCI
  https://wiki.osdev.org/UEFI
  https://wiki.osdev.org/VGA_Hardware
  https://wiki.osdev.org/X86-64_Instruction_Encoding
  https://wiki.osdev.org/ELF_Tutorial

  https://en.uncyclopedia.co/
  https://en.uncyclopedia.co/wiki/Uncyclopedia:Best_of
  https://en.uncyclopedia.co/wiki/Antbortion
  https://en.uncyclopedia.co/wiki/Bouncy_Castle
  https://en.uncyclopedia.co/wiki/Printer_Ink
  https://en.uncyclopedia.co/wiki/Ajit_Pai
  https://en.uncyclopedia.co/wiki/Art%27s_True_Meanings
  https://en.uncyclopedia.co/wiki/Joseph_Stalin
  https://en.uncyclopedia.co/wiki/Reddit
  https://en.uncyclopedia.co/wiki/Great_Pacific_Cum_Patch
  https://en.uncyclopedia.co/wiki/Prohibition
  https://en.uncyclopedia.co/wiki/Holocaust
  https://en.uncyclopedia.co/wiki/Reductio_ad_Hitlerum
  https://en.uncyclopedia.co/wiki/History_of_Great_Britain
  https://en.uncyclopedia.co/wiki/Long_article
  https://en.uncyclopedia.co/wiki/HowTo:Survive_a_Zombie_Outbreak
  https://en.uncyclopedia.co/wiki/Holy_Roman_Empire
  https://en.uncyclopedia.co/wiki/Crimes_inspired_by_video_games  

  https://encyclopediadramatica.online/Main_Page
  https://encyclopediadramatica.online/9/11
  https://encyclopediadramatica.online/9gag
  https://encyclopediadramatica.online/A_frank_discussion_of_mental_illness
  https://encyclopediadramatica.online/Adolf_Hitler
  https://encyclopediadramatica.online/Asperger%27s_Syndrome
  https://encyclopediadramatica.online/Category:Featured_Articles
  https://encyclopediadramatica.online/Portal:Wikipedia
  https://encyclopediadramatica.online/16-year-old_girl
  https://encyclopediadramatica.online/4chan
  https://encyclopediadramatica.online/Reddit
  https://encyclopediadramatica.online/Pepe
  https://encyclopediadramatica.online/Desu
  https://encyclopediadramatica.online/High_Score
  https://encyclopediadramatica.online/GamerGate
  https://encyclopediadramatica.online/Brenton_Tarrant
  https://encyclopediadramatica.online/Anders_Behring_Breiviki
  https://encyclopediadramatica.online/Nigger

  https://www.electricuniverse.info/electric-universe-theory/
  https://www.electricuniverse.info/electric-universe-model-on-wikipedia/

  http://www.scpwiki.com/
  http://www.scpwiki.com/top-rated-pages
  http://www.scpwiki.com/scp-173
  http://www.scpwiki.com/scp-049
  http://www.scpwiki.com/scp-055
  http://www.scpwiki.com/scp-087
  http://www.scpwiki.com/scp-682
  http://www.scpwiki.com/scp-096

  https://esolangs.org/wiki/Main_Page
  https://esolangs.org/wiki/Esoteric_programming_language
  https://esolangs.org/wiki/Hello_world_program_in_esoteric_languages
  https://esolangs.org/wiki/Brainfuck
  https://esolangs.org/wiki/Deadfish
  https://esolangs.org/wiki/Imaginary_function
  https://esolangs.org/wiki/Transfinite_program
  https://esolangs.org/wiki/Category:Concepts
  https://esolangs.org/wiki/Tarpit
  https://esolangs.org/wiki/List_of_ideas
  https://esolangs.org/wiki/Omgrofl
  https://esolangs.org/wiki/Brainfuck_algorithms

  https://en.wikiquote.org/wiki/Noam_Chomsky
  https://en.wikiquote.org/wiki/Albert_Einstein
  https://en.wikiquote.org/wiki/Adolf_Hitler
  https://en.wikiquote.org/wiki/Mahatma_Gandhi
  https://en.wikiquote.org/wiki/Leo_Tolstoy
  https://en.wikiquote.org/wiki/Peter_Kropotkin
  https://en.wikiquote.org/wiki/Diogenes_of_Sinope

  https://www.wikidata.org/wiki/Wikidata:Main_Page
  https://www.wikidata.org/wiki/Q2

  http://rosettacode.org/wiki/Rosetta_Code
  http://rosettacode.org/wiki/Determine_if_a_string_is_numeric

  http://progopedia.com/about/
  http://progopedia.com/example/factorial/

  https://www.gutenberg.org/
  https://www.gutenberg.org/files/201/201-h/201-h.htm
  https://www.gutenberg.org/files/11/11-h/11-h.htm
  http://gutenberg.org/files/10/10-h/10-h.htm
  http://gutenberg.org/cache/epub/2800/pg2800-images.html
  http://gutenberg.org/files/2000/2000-h/2000-h.htm
  http://gutenberg.org/files/2701/2701-h/2701-h.htm
  https://www.gutenberg.org/files/29419/29419-h/29419-h.htm
  https://www.gutenberg.org/files/1342/1342-h/1342-h.htm
  http://www.gutenberg.org/cache/epub/61/pg61-images.html
  https://www.gutenberg.org/files/16328/16328-h/16328-h.htm
  https://www.gutenberg.org/files/5200/5200-h/5200-h.htm
  https://www.gutenberg.org/files/1228/1228-h/1228-h.htm
  http://www.gutenberg.org/cache/epub/5768/pg5768-images.html
  https://www.gutenberg.org/files/63232/63232-h/63232-h.htm
  https://www.gutenberg.org/files/35461/35461-h/35461-h.htm
  https://www.gutenberg.org/files/56796/56796-h/56796-h.htm
  https://www.gutenberg.org/files/20190/20190-h/20190-h.htm
  https://www.gutenberg.org/files/41983/41983-h/41983-h.htm
  https://www.gutenberg.org/files/53881/53881-0.txt
  https://www.gutenberg.org/files/45849/45849-h/45849-h.htm
  https://www.gutenberg.org/files/2298/2298-h/2298-h.htm
  https://www.gutenberg.org/files/25267/25267-h/25267-h.htm
  https://www.gutenberg.org/files/35937/35937-h/35937-h.htm
  https://www.gutenberg.org/files/23428/23428-h/23428-h.htm
  http://www.gutenberg.org/cache/epub/29765/pg29765.txt
  https://www.gutenberg.org/files/7787/7787-h/7787-h.htm
  http://www.gutenberg.org/cache/epub/15127/pg15127-images.html
  http://www.gutenberg.org/cache/epub/16713/pg16713.txt
  http://www.gutenberg.org/cache/epub/634/pg634-images.html
  https://www.gutenberg.org/files/28553/28553-h/28553-h.htm
  https://www.gutenberg.org/files/11039/11039-h/11039-h.htm
  https://www.gutenberg.org/files/24575/24575-h/24575-h.htm
  https://www.gutenberg.org/files/60793/60793-h/60793-h.htm
  https://www.gutenberg.org/files/12342/12342-h/12342-h.htm 

  https://wiki.archlinux.org/

  https://half-life.fandom.com/wiki/Timeline_of_the_Half-Life_universe

  http://www.catb.org/esr/faqs/hacker-howto.html
  http://www.catb.org/~esr/jargon/html/index.html
  http://www.catb.org/~esr/writings/taoup/html

  https://en.wikisource.org/wiki/The_Pig_and_the_Box
  https://en.wikisource.org/wiki/An_Anarchist_FAQ/What_is_anarchism%3F
  https://en.wikisource.org/wiki/The_Country_of_the_Blind

  https://libregamewiki.org
  https://libregamewiki.org/Xonotic

  https://www.archiveteam.org/index.php?title=Main_Page
  https://www.archiveteam.org/index.php?title=Deathwatch
  https://www.archiveteam.org/index.php?title=Alive..._OR_ARE_THEY

  https://wikiindex.org/Welcome
  https://wikiindex.org/List_of_wikis_by_number_of_pages

  https://allthetropes.org/wiki/Main_Page
  https://allthetropes.org/wiki/Absurdly_High_Level_Cap
  https://allthetropes.org/wiki/All_The_Tropes:This_Index_Is_Not_an_Example
  https://allthetropes.org/wiki/Damn_You,_Muscle_Memory!
  https://allthetropes.org/wiki/Non-Indicative_Name
  https://allthetropes.org/wiki/Misblamed
  https://allthetropes.org/wiki/Crazy_Prepared
  https://allthetropes.org/wiki/Product_Placement
  https://allthetropes.org/wiki/Mundane_Utility
  https://allthetropes.org/wiki/Reality_Is_Unrealistic
  https://allthetropes.org/wiki/Conspiracy_Theories
  https://allthetropes.org/wiki/Special_Effect_Failure
  https://allthetropes.org/wiki/Multiple_Endings
  https://allthetropes.org/wiki/Game_Breaking_Bug
  https://allthetropes.org/wiki/Informed_Ability
  https://allthetropes.org/wiki/Not_So_Different
  https://allthetropes.org/wiki/Stable_Time_Loop
  https://allthetropes.org/wiki/Translation_Convention
  https://allthetropes.org/wiki/National_Stereotypes
  https://allthetropes.org/wiki/Unfortunate_Names
  https://allthetropes.org/wiki/Talking_to_Himself
  https://allthetropes.org/wiki/It%27s_Popular,_Now_It_Sucks
  https://allthetropes.org/wiki/Authority_Equals_Asskicking

  https://wiki.p2pfoundation.net/Main_Page

  https://freedomdefined.org/Definition

  http://www.talisman.org/unix/burgess-guide.html
  http://www.rbs0.com/chist.htm
  http://www.don-lindsay-archive.org/skeptic/arguments.html
  https://www.gilesorr.com/wm/table.html
  http://members.tripod.com/quantum_mars/
  http://keithlynch.net/timeline.html

  http://collapseos.org/
  http://collapseos.org/forth.html
  http://collapseos.org/why.html

  http://oeis.org/wiki/Welcome

  http://www.macroevolution.net/ape-human-hybrids.html

  https://jcdverha.home.xs4all.nl/scijokes/index.html
  https://jcdverha.home.xs4all.nl/scijokes/1.html
  https://jcdverha.home.xs4all.nl/scijokes/1_1.html
  https://jcdverha.home.xs4all.nl/scijokes/2_5.html
  https://jcdverha.home.xs4all.nl/scijokes/2.html
  https://jcdverha.home.xs4all.nl/scijokes/9.html
  https://jcdverha.home.xs4all.nl/scijokes/10.html
  https://jcdverha.home.xs4all.nl/scijokes/1_5.html
  https://jcdverha.home.xs4all.nl/scijokes/6.html
  https://jcdverha.home.xs4all.nl/scijokes/8_7.html
  https://jcdverha.home.xs4all.nl/scijokes/4.html

  https://wiki.installgentoo.com/wiki/Software_minimalism
  https://wiki.installgentoo.com/wiki/Programming_languages
  https://wiki.installgentoo.com/wiki/Build_a_PC

  http://www.lord-enki.net/stories.html
  http://www.lord-enki.net/ideas.html

  https://freemedia.neocities.org/

  https://www.lostmediawiki.com/The_Basement_Tapes_(partially_found_Columbine_killers_video_diary;_1999)
  
  https://s23.org/wiki/Main_Page

  https://wiki.freephile.org/wiki/Bash

  https://www.stallman.org/
  https://www.stallman.org/glossary.html

  https://archive.org/download/CestaDoSpojenchSttSeveroamerickch/CestaDoSpojenychSttSeveroamerickch.pdf
  https://ia801009.us.archive.org/34/items/ohvezdach/ohvezdach.pdf
  https://ia800708.us.archive.org/33/items/ChroniclesOfAtys/chronicles%20of%20atys.pdf
  https://ia801005.us.archive.org/23/items/noncompetitivesociety/non-competitive%20society.pdf
  https://ia802808.us.archive.org/21/items/figosdev_users_Fsf2/fsf_third_draft.pdf

  https://gitlab.com/drummyfish/my_text_data/-/raw/master/cheatsheets/history%20cheatsheet.txt
  https://gitlab.com/drummyfish/my_text_data/-/raw/master/cheatsheets/bash%20cheatsheet.txt
  https://gitlab.com/drummyfish/my_text_data/-/raw/master/cheatsheets/esperanto%20cheatesheet.txt
  https://gitlab.com/drummyfish/my_text_data/-/raw/master/cheatsheets/computer%20history%20cheatsheet.txt

  http://www.survivorlibrary.com/library/aid-to-survival.pdf
  http://www.survivorlibrary.com/library/wilderness-survival-skills.pdf

  http://www.dii.uchile.cl/~daespino/files/Iso_C_1999_definition.pdf

  http://textfiles.com/100/famous.bug
  http://textfiles.com/100/feh-1
  http://textfiles.com/100/taoprogram.pro

  http://bitcheese.net/
  http://bitcheese.net/code/delimiters-must-die
  http://bitcheese.net/web_browsers_must_die

  https://en.citizendium.org/wiki/Welcome_to_Citizendium
  https://en.citizendium.org/wiki/Citizendium
  https://en.citizendium.org/wiki/Life
  https://en.citizendium.org/wiki/DNA
  https://en.citizendium.org/wiki/Fractal
  https://en.citizendium.org/wiki/Scientific_method
  https://en.citizendium.org/wiki/DNA

  https://tldp.org/LDP/GNU-Linux-Tools-Summary/html/GNU-Linux-Tools-Summary.html
  https://tldp.org/LDP/Linux-Filesystem-Hierarchy/html/Linux-Filesystem-Hierarchy.html
  https://tldp.org/LDP/lfs/LFS-BOOK-6.1.1-NOCHUNKS.html

  https://en.wikisource.org/wiki/1911_Encyclop%C3%A6dia_Britannica/Nebular_Theory
  https://en.wikisource.org/wiki/1911_Encyclop%C3%A6dia_Britannica/Atom

  https://www.tastyfish.cz/lrs/lrs_wiki_full.html

  https://tilde.town/
  https://solar.lowtechmagazine.com/
  https://suckless.org/philosophy/
  http://suckless.org/rocks/
  http://suckless.org/sucks/
  http://quotes.cat-v.org/
  http://www.tastyfish.cz/
  https://frogesay.neocities.org/
  http://countercomplex.blogspot.com/
  https://lukesmith.xyz/
  http://keithlynch.net/history.net.html
  https://freddie.witherden.org/pages/font-rasterisation/
  http://motherfuckingwebsite.com/
  http://tauceti.org/computinghistory.pdf
  http://idallen.org/
  http://www.factsbehindfaith.com/default.aspx?intContentID=31
  https://www.fractalus.com/fractal-art-faq/faq04.html
  http://faq.thekrib.com/begin-stress.html
  http://www.nuclearfaq.ca/cnf_sectionD.htm
  http://ioccc.org/all/summary.txt
  http://runtimeterror.com/tech/lil/
  https://www.erzo.org/shannon/writing/csua/encyclopedia.html

  https://web.archive.org/web/20050413045545/http://www.wikifaq.com/Llamas_and_Alpacas_FAQs

  http://archive.li/x3zYl
  https://archive.li/72WO3
  https://archive.li/UBfhb
  https://archive.li/ASH3

  http://eng.anarchopedia.org/Anarcho-Communism
  http://eng.anarchopedia.org/Criticism_of_Wikipedia
  http://eng.anarchopedia.org/capitalism

  https://linux.die.net/man/1/gcc
  https://linux.die.net/man/7/unicode
  https://linux.die.net/man/1/awk

  https://www.chessprogramming.org/Main_Page
  https://www.chessprogramming.org/Neural_Networks
  https://www.chessprogramming.org/Deep_Learning
  https://www.chessprogramming.org/Stockfish

  https://commons.wikimedia.org/wiki/Main_Page
  https://upload.wikimedia.org/wikipedia/commons/a/a4/United_states.svg
  https://upload.wikimedia.org/wikipedia/commons/8/8d/Europe-sv.svg
  https://upload.wikimedia.org/wikipedia/commons/3/3f/CIA_WorldFactBook-Political_world.svg
  https://upload.wikimedia.org/wikipedia/commons/8/89/Map_solar_system.svg
  https://upload.wikimedia.org/wikipedia/commons/4/4d/Periodic_table_large.svg
  https://upload.wikimedia.org/wikipedia/commons/5/58/Organ_systems_cy.svg
  https://upload.wikimedia.org/wikipedia/commons/d/dd/Full_Moon_Luc_Viatour.jpg
  https://upload.wikimedia.org/wikipedia/commons/5/5b/Linux_kernel_map.png
  https://upload.wikimedia.org/wikipedia/commons/b/bc/Czech-regions.svg
  https://upload.wikimedia.org/wikipedia/commons/c/cf/EM_Spectrum_Properties_edit.svg
  https://upload.wikimedia.org/wikipedia/commons/f/fe/M4A4_cutaway.svg
  https://upload.wikimedia.org/wikipedia/commons/9/9b/No._COSMOS_%28star-chart%29_black.png

  https://combineoverwiki.net/wiki/Combine
  https://combineoverwiki.net/wiki/Combine_Advisor
  https://combineoverwiki.net/wiki/Wallace_Breen
  https://combineoverwiki.net/wiki/Dr._Gordon_Freeman
  https://combineoverwiki.net/wiki/Timeline_of_the_Half-Life_universe
  https://combineoverwiki.net/wiki/G_Man
  https://combineoverwiki.net/wiki/Nihilanth

  https://www.britannica.com/sports/Olympic-Games
  https://www.britannica.com/place/ancient-Greece

  http://tenant.net/Community/steal/steal.html#2.01.0
  https://solar.lowtechmagazine.com/2007/12/email-in-the-18.html
  https://solar.lowtechmagazine.com/2021/10/how-to-build-a-low-tech-solar-panel.html
  https://solar.lowtechmagazine.com/2021/05/how-to-design-a-sailing-ship-for-the-21st-century.html

  http://humanknowledge.net/Thoughts.html#CosmologyMisunderstandings
  https://www.zakon.org/robert/internet/timeline/

  https://www.humanbiologicaldiversity.com/

  http://biostatisticien.eu/www.searchlores.org/deepweb_searching.htm
  " | shuf | wget -i - -E -e robots=off -nc -nd -U "Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.1; SV1)" --tries=3 -k -w 1 -P offline

echo "minimizing HTML files"

for f in offline/*.html; do
  sed ":a;N;$!ba;s/>\s*</></g" "$f" > tmp.txt
  mv tmp.txt $f
done
