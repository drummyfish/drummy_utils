#include "model.h"

signed char compareSequences(const unsigned short *s1, const unsigned short *s2)
{
  for (int i = 0; i < PREVIOUS_WORDS; ++i)
  {
    if (*s1 > *s2)
      return 1;
    else if (*s1 < *s2)
      return -1;

    s1++;
    s2++;
  }

  return 0;
}

char getSequenceBounds(const unsigned short *sequence, 
  unsigned short *indexFrom, 
  unsigned short *indexTo)
{
  int lower = 0;
  int upper = SEQUENCES - 1;
  int index;

  while (1) // binary search
  {
    if (lower > upper)
      return 0;

    index = (upper + lower) / 2;

    signed char comparison = compareSequences(sequence,sequences + (PREVIOUS_WORDS + 1) * index);

    if (comparison == 0)
      break;
    else if (comparison == -1)
      upper = index - 1;
    else
      lower = index + 1;
  }

  lower = index;

  while (lower >= 0 && compareSequences(sequence,sequences + (PREVIOUS_WORDS + 1) * lower) == 0)
    lower--;

  lower++;

  upper = index;

  while (upper < (SEQUENCES * (PREVIOUS_WORDS + 1)) && compareSequences(sequence,sequences + (PREVIOUS_WORDS + 1) * upper) == 0)
    upper++;

  upper--;

  *indexFrom = lower;
  *indexTo = upper;

  return 1;
}

unsigned short getNextSymbol(const unsigned short *previousSymbols, int randomNumber)
{
  unsigned short i0, i1;

  if (getSequenceBounds(previousSymbols,&i0,&i1))
  {
    unsigned short i = i0 + randomNumber % (i1 - i0 + 1);

    return sequences[(PREVIOUS_WORDS + 1) * i + PREVIOUS_WORDS];
  }

  return 0;
}
