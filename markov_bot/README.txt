Quick and dirty Markov chain text generator.

Paste a train text as a single long C string into a text file. Include it at the
top of train.c. Compile and run train.c, it outputs model.h. Then compile and run 
generate.c

released under CC0 1.0, public domain.
