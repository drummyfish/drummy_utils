#include <stdio.h>
#include <stdlib.h>
#include "generator.h"

void printSentence(int randomNumber)
{
  srand(randomNumber);

  unsigned short s[PREVIOUS_WORDS];

  for (int i = 0; i < PREVIOUS_WORDS; ++i)
    s[i] = 0;

  int wordCount = 0;

  int first = 1;

  while (wordCount < 1000)
  {
    unsigned short next = getNextSymbol(s,rand());

    if (next == 0)
      break;

    if (first)
    {
      if (words[next][0] >= 'a' && words[next][0] <= 'z')
        printf("%c%s",words[next][0] - ('a' - 'A'),words[next] + 1);
      else
        printf("%s",words[next]);

      first = 0;
    }
    else
    {
      if (words[next][1] != 0 || !(
        words[next][0] == '.' ||
        words[next][0] == ',' ||
        words[next][0] == '!' ||
        words[next][0] == '?' ||
        words[next][0] == ')' ||
        words[next][0] == ';' ||
        words[next][0] == ':' ||
        words[next][0] == '\''))
        putchar(' ');

      if (words[next][0] == 'i' && words[next][1] == 0)
        putchar('I');
      else   
        printf("%s",words[next]);
    }

    for (int i = 0; i < PREVIOUS_WORDS - 1; ++i)
      s[i] = s[i + 1];

    s[PREVIOUS_WORDS - 1] = next;

    wordCount++;
  }

  putchar('\n');
}

int main(void)
{
  for (int i = 0; i < 200; ++i)
    printSentence(i);

  return 0;
}
