#include <stdio.h>
#include <stdint.h>

const char trainText[] =

#include "textFlatland.txt"

;

#define TRAIN_TEXT_LENGTH (sizeof(trainText) - 1)

#define CHAR_IS_TERMINATOR(c) ((c) == 0 || (c) == '.' || (c) == '?' || (c) == '!')
#define CHAR_IS_INTERPUNCTION(c) (CHAR_IS_TERMINATOR(c) || c == ',' || c == '"' || c == ';' || c == ':' || c == ')' || c == '(' || c == '[' || c == ']')

#define WORD_MAX_LENGTH 31
#define MAX_WORDS 5000

#define PREVIOUS_WORDS 2

unsigned int wordCount = 0;
char words[MAX_WORDS][WORD_MAX_LENGTH + 1];

#define MAX_SEQUENCES 100000

unsigned int sequenceCount = 0;

typedef struct
{
  unsigned int previousWords[PREVIOUS_WORDS];
  unsigned int currentWord;
  unsigned int frequency;
} SequenceRecord;

SequenceRecord sequences[MAX_SEQUENCES * (PREVIOUS_WORDS + 2)];

int getWordIndex(const char *word)
{
  for (int i = 0; i < wordCount; ++i)
  {
    int matches = 1;

    const char *word2 = words[i];

    for (int i = 0; i < WORD_MAX_LENGTH; ++i)
    {
      if (word[i] != word2[i])
      {
        matches = 0;
        break;
      }

      if (word[i] == 0)
        break;
    }

    if (matches)
      return i;
  }

  return -1;
}

int foundWord(const char *word)
{
  int index = getWordIndex(word);

  if (index < 0 && wordCount < MAX_WORDS)
  {
    int p = 0;

    while (*word != 0)
    {
      words[wordCount][p] = *word;
      word++;
      p++;
    }

    words[wordCount][p] = 0;

    index = wordCount;

    wordCount++;
  }

  return index + 1;
}

void printWordArray(void)
{
  printf("#define WORDS %d\n",wordCount);

  puts("const char *words[WORDS] = {\n\"\", // 0, unused");

  for (int i = 0; i < wordCount; ++i)
  {
    const char *s = words[i];

    putchar('"');
 
    while (*s != 0)
    { 
      if (*s != '"')
        putchar(*s);
      else
      {
        putchar('\\');
        putchar('"');
      }

      s++;
    }

    putchar('"');

    if (i != wordCount - 1)
      putchar(',');

    printf(" // %d\n",i + 1);
  }

  puts("};");
}

void printWord(unsigned int wordNumber)
{
  printf("%s",wordNumber > 0 ? words[wordNumber - 1] : "NONE");
}

void printSequences(void)
{
  for (int i = 0; i < sequenceCount; ++i)
  {
    printf("%d: ",i);
    for (int j = 0; j < PREVIOUS_WORDS; ++j)
    {
      printWord(sequences[i].previousWords[j]);

      putchar(' ');
    }

    printf("-> ");

    printWord(sequences[i].currentWord);

    printf(" (%d)\n",sequences[i].frequency);
  }
}

void foundSequence(const unsigned int *sequence)
{
  int found = 0;

  for (int i = 0; i < sequenceCount; ++i)
  {
    found = 1;

    if (sequence[PREVIOUS_WORDS] != sequences[i].currentWord)
      found = 0;
    else
      for (int j = 0; j < PREVIOUS_WORDS; ++j)
        if (sequence[j] != sequences[i].previousWords[j])
        {
          found = 0;
          break;
        }

    if (found)
    {
      sequences[i].frequency++;
      break;
    }
  }

  if (!found && sequenceCount < MAX_SEQUENCES)
  {
    for (int i = 0; i < PREVIOUS_WORDS; ++i)
      sequences[sequenceCount].previousWords[i] = sequence[i];

    sequences[sequenceCount].currentWord = sequence[PREVIOUS_WORDS];
    sequences[sequenceCount].frequency = 1;

    sequenceCount++;
  }
}

void printSequenceArray()
{
  int total = 0;

  for (int i = 0; i < sequenceCount; ++i)
    total += sequences[i].frequency;

  printf("#define SEQUENCES %d\n",total);
  puts("const unsigned short sequences[SEQUENCES * (PREVIOUS_WORDS + 1)] = {");

  for (int i = 0; i < sequenceCount; ++i)
  {
    for (int j = 0; j < sequences[i].frequency; ++j)
    {
      for (int k = 0; k < PREVIOUS_WORDS; ++k)
        printf("%d,",sequences[i].previousWords[k]);

      printf("%d",sequences[i].currentWord);

      if (i != sequenceCount - 1 || j != sequences[i].frequency - 1)
        putchar(',');

      printf(" // ");

      for (int k = 0; k < PREVIOUS_WORDS; ++k)
      {
        printWord(sequences[i].previousWords[k]);
        putchar(' ');
      }

      printf("-> ");
      printWord(sequences[i].currentWord);
      putchar('\n');
    }
  }

  puts("};");
}

void handleSequence(unsigned int *sequence, int wordNumber)
{
  for (int i = 0; i < PREVIOUS_WORDS; ++i)
    sequence[i] = sequence[i + 1];

  sequence[PREVIOUS_WORDS] = wordNumber;

  foundSequence(sequence);
}

void sortSequences(void)
{
  for (int i = sequenceCount - 2; i >= 0; --i)
    for (int j = 0; j <= i; ++j)
    {
      int greater = -1;

      for (int k = 0; k < PREVIOUS_WORDS; ++k)
        if (sequences[j].previousWords[k] > sequences[j + 1].previousWords[k])
        {
          greater = 1;
          break;
        }
        else if (sequences[j].previousWords[k] < sequences[j + 1].previousWords[k])
        {
          greater = 0;
          break;
        }

      if (greater == -1)
      {
        if (sequences[j].frequency < sequences[j + 1].frequency)
          greater = 1;
        else if (sequences[j].frequency > sequences[j + 1].frequency)
          greater = 0;
      }

      if (greater == 1)
      { 
        SequenceRecord r = sequences[j + 1];
        sequences[j + 1] = sequences[j];
        sequences[j] = r;
      } 
    }
}

void printModel(void)
{
  printf("#define PREVIOUS_WORDS %d\n\n",PREVIOUS_WORDS);

  printWordArray();
  putchar('\n');
  printSequenceArray();
}

int main(void)
{
  // make the word table:

  char word[64];
  int wordPos = 0;

  unsigned int context[PREVIOUS_WORDS + 1];

  for (int i = 0; i < PREVIOUS_WORDS + 1; ++i)
    context[i] = 0;

  for (int i = 0; i < TRAIN_TEXT_LENGTH; ++i)
  {
    char c = trainText[i];

    if (c > 127)
      c = ' ';
    else if (c >= 'A' && c <= 'Z')
      c += 'a' - 'A';

    int interpunction = CHAR_IS_INTERPUNCTION(c);
    int terminator = CHAR_IS_TERMINATOR(c) || i == TRAIN_TEXT_LENGTH - 1;

    if (c == ' ' || c == '\n' || c == '\t' || c == 0 || interpunction || terminator)
    {
      if (wordPos > WORD_MAX_LENGTH)
        wordPos = WORD_MAX_LENGTH;

      word[wordPos] = 0;

      if (wordPos != 0)
        handleSequence(context,foundWord(word));

      wordPos = 0;

      if (interpunction || terminator)
      {
        word[0] = c;
        word[1] = 0;
        handleSequence(context,foundWord(word));

        if (terminator)
        {
          handleSequence(context,0);

          for (int i = 0; i < PREVIOUS_WORDS + 1; ++i)
            context[i] = 0;
        }
      }
    }
    else
    {
      word[wordPos] = c;
      wordPos++;
    }
  }

  sortSequences();

  printModel();

  return 0;
}
