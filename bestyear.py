# Script to determine what the best year was for me.
#
# by drummyfish, released under CC0 1.0

YEAR_START = 1980
YEAR_END = 2020

influenceA = ( # for most normal, positive events
  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0.1,0.4,
  1.0,
  0.8,0.6,0.5,0.4,0.4,0.4,0.3,0.3,0.3,0.2,0.2,0.2,0.2,0.1,0.1,0.1,0.1
  )

influenceB = ( # for negative, progressively worse events
  0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
  0.1,
  0.1,0.2,0.2,0.3,0.3,0.4,0.5,0.6,0.7,0.8,0.9,1.0,1.0,0.9,0.8,0.6,0.4,0.3,0.2,0.1
  )

events = (
  (1000,"dummy",0,influenceA),
  (1976,"Na Samote u Lesa",1.0,influenceA),
  (1981,"Indiana Jones",1.0,influenceA),
  (1982,"Rambo",0.4,influenceA),
  (1983,"Slunce Seno",0.8,influenceA),
  (1984,"Neverending Story",0.5,influenceA),
  (1985,"Back to the Future",0.9,influenceA),
  (1985,"Vesnicko ma Strediskova",0.5,influenceA),
  (1986,"Crocodile Dundee",0.5,influenceA),
  (1985,"Prince of Persia",0.7,influenceA),
  (1987,"Duck Tales",0.4,influenceA),
  (1989,"GameBoy",0.7,influenceA),
  (1989,"Havel",0.9,influenceA),
  (1989,"Kickboxer",0.5,influenceA),
  (1989,"WWW",2.0,influenceA),
  (1991,"Silence of the Lambs",0.6,influenceA),
  (1991,"Lemmings",0.4,influenceA),
  (1991,"Duke Nukem",0.8,influenceA),
  (1992,"Reservoir Dogs",0.5,influenceA),
  (1992,"Wolf 3D",0.8,influenceA),
  (1992,"Dedictvi",0.9,influenceA),
  (1992,"Mortal Combat",0.6,influenceA),
  (1993,"Doom",1.9,influenceA),
  (1993,"Jurrasic Park",1.0,influenceA),
  (1993,"Prehistorik 2",0.6,influenceA),
  (1993,"Les Visiteurs",0.9,influenceA),
  (1994,"Doom II",0.5,influenceA),
  (1994,"Ace Ventura",0.4,influenceA),
  (1994,"Pulp Fiction",1.1,influenceA),
  (1994,"Forrest Gump",0.9,influenceA),
  (1994,"Lion King",1.0,influenceA),
  (1994,"Tekken",0.3,influenceA),
  (1994,"Friends",0.5,influenceA),
  (1995,"Play Station",0.7,influenceA),
  (1995,"Dragon History",0.7,influenceA),
  (1995,"Warcraft II",0.6,influenceA),
  (1995,"Toy Story",0.4,influenceA),
  (1996,"Duke Nukem 3D",0.8,influenceA),
  (1996,"Quake",1.0,influenceA),
  (1996,"Crash Bandicoot",0.6,influenceA),
  (1997,"Jurrasic Park 2",0.3,influenceA),
  (1997,"Atomic Bomberman",0.4,influenceA),
  (1997,"Age of Empires",0.4,influenceA),
  (1997,"Blood",0.5,influenceA),
  (1997,"Diablo",0.4,influenceA),
  (1997,"Shadow Warrior",0.5,influenceA),
  (1997,"GTA",0.7,influenceA),
  (1997,"Quake II",0.4,influenceA),
  (1997,"Pokemon",1.7,influenceA),
  (1997,"Harry Potter",1.4,influenceA),
  (1997,"UH flood",0.3,influenceA),
  (1997,"Jackie Brown",0.3,influenceA),
  (1997,"Titanic",0.3,influenceA),
  (1997,"Tamagotchi",0.5,influenceA),
  (1997,"Digimon",0.3,influenceA),
  (1998,"Half-Life",1.2,influenceA),
  (1998,"Furby",0.4,influenceA),
  (1998,"Oggy and the Cockroaches",0.2,influenceA),
  (1998,"Gates of Skeldal",0.7,influenceA),
  (1998,"GameBoy Color",0.9,influenceA),
  (1998,"Quake III Arena",0.9,influenceA),
  (1998,"Pokemon RBY",1.3,influenceA),
  (1998,"Big Lebowski",0.2,influenceA),
  (1998,"Les Visiteurs II",0.5,influenceA),
  (1998,"Polda",0.5,influenceA),
  (1999,"GTA 2",0.3,influenceA),
  (1999,"new Star Wars",0.3,influenceA),
  (1999,"Worms Armageddon",0.4,influenceA),
  (1999,"HOMAM III",0.7,influenceA),
  (1999,"Pelisky",0.9,influenceA),
  (1999,"Counter Strike",0.4,influenceA),
  (2000,"Linkin Part",1.0,influenceA),
  (2000,"Nokia 3310",0.6,influenceA),
  (2000,"Gladiator",0.3,influenceA),
  (2000,"Chicken Run",0.6,influenceA),
  (2000,"Web 2.0",-1.4,influenceB),
  (2001,"GameBoy Advance",0.8,influenceA),
  (2001,"Hannibal",0.8,influenceA),
  (2001,"Creative Commons",0.7,influenceA),
  (2001,"Wikipedia",0.7,influenceA),
  (2001,"Advance Wars",0.5,influenceA),
  (2001,"Pokemon GSC",1.0,influenceA),
  (2001,"GTA III",1.2,influenceA),
  (2001,"Crocodile Dundee III",0.3,influenceA),
  (2001,"Jurrasic Park 3",0.1,influenceA),
  (2002,"Warcraft III",1.1,influenceA),
  (2002,"GTA Vice City",1.0,influenceA),
  (2002,"Mafia",1.0,influenceA),
  (2002,"TES Morrowind",1.7,influenceA),
  (2003,"Warcraft III: TFT",0.3,influenceA),
  (2003,"4Chan",0.4,influenceA),
  (2003,"Pokemon RSE",0.7,influenceA),
  (2003,"Steam",-0.9,influenceB),
  (2003,"LOTR movie",0.9,influenceA),
  (2003,"Kill Bill",0.1,influenceA),
  (2003,"Nokia 6600",0.5,influenceA),
  (2004,"Half-Life 2",1.4,influenceA),
  (2004,"World of Warcraft",1.5,influenceA),
  (2004,"Facebook",-1.1,influenceB),
  (2004,"GTA San Andreas",1.2,influenceA),
  (2004,"Doom 3",0.9,influenceA),
  (2004,"Far Cry",0.2,influenceA),
  (2005,"YouTube",0.8,influenceB),
  (2005,"Reddit",0.8,influenceB),
  (2006,"Windows Vista",-0.3,influenceB),
  (2006,"TES Oblivion",0.8,influenceA),
  (2006,"Twitter",-0.5,influenceB),
  (2007,"iPhone (Nokia death)",-0.8,influenceB),
  (2008,"GTA IV",0.8,influenceA),
  (2008,"Android",-0.2,influenceB),
  (2008,"Trackmania Nations Forever",1.0,influenceA),
  (2008,"Twilight",0.4,influenceA),
  (2009,"Inglorious Basterds",-0.2,influenceB),
  (2008,"Intel ME",-0.5,influenceB),
  (2009,"Avatar",-0.5,influenceB),
  (2010,"Mafia II",-0.4,influenceB),
  (2011,"TES Skyrim",0.9,influenceA),
  (2011,"Havel dies",-0.5,influenceB),
  (2012,"Avengers",-0.8,influenceB),
  (2012,"Legend of Grimrock",0.5,influenceA),
  (2012,"PewDiePie",-0.8,influenceB),
  (2013,"GTA V",0.7,influenceA),
  (2013,"Zeman President",-1.0,influenceB),
  (2014,"Legend of Grimrock II",0.6,influenceA),
  (2014,"TES Online",-0.5,influenceB),
  (2015,"Windows 10",-0.7,influenceB),
  (2015,"Arduboy",0.5,influenceA),
  (2016,"Doom (2016)",0.7,influenceA),
  (2016,"Mafia III",-0.6,influenceB),
  (2016,"Les Visiteurs III",-0.1,influenceB),
  (2017,"Trump",-0.9,influenceB),
  (2017,"Babis",-0.5,influenceB),
  (2017,"Pokitto",0.4,influenceA),
  (2017,"Quake Champions",-0.6,influenceB),
  (2018,"PeerTube",0.8,influenceA),
  (2018,"Jurrasic Park 3",0.2,influenceA),
  (2018,"Gamebuino Meta",0.4,influenceA),
  (2018,"Greta Thunberg",0.7,influenceA),
  (2020,"Corona",0.2,influenceA),
  (2017,"Warcraft Reforged",-0.9,influenceB),
  )

yearScore = [0 for i in range(YEAR_START,YEAR_END + 1)]

for e in events:
  start = e[0] - YEAR_START - int(len(e[3]) / 2)
  end = start + len(e[3])

  i = 0
  pos = start

  while pos < end:
    if pos >= len(yearScore):
      break

    if pos >= 0:
      yearScore[pos] += e[3][i] * e[2]

    i += 1
    pos += 1

year = YEAR_START

bestYear = 0
worstYear = 0

for i in range(len(yearScore)):
  things = ""

  first = True

  for e in events:
    if e[0] == year:
      if not first:
        things += ", "
      else:
        first = False

      things += e[1]

  print(str(year) + ": " + str(yearScore[i]).ljust(8) + " " + things)

  if yearScore[i] > yearScore[bestYear]:
    bestYear = i

  if yearScore[i] < yearScore[worstYear]:
    worstYear = i
    
  year += 1

print("\nbest year: " + str(YEAR_START + bestYear) + "\nworst year: " + str(YEAR_START + worstYear))

