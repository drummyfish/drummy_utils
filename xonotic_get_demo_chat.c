/* Dirty program that tries to extract chat from Xonotic demo files. */

#include <stdio.h>

#define NICK_MAX_LEN 40
#define NICK_MIN_LEN 2
#define NICK_MAX_BAD_CHARS 4
#define TOTAL_MAX_BAD_CHARS 6
#define TOTAL_MIN_LEN 10

#define BUFFER_SIZE 1024
char buffer[BUFFER_SIZE];

int totalChars, badChars, state = 0;

int main(int argc, char **argv)
{
  FILE *f = fopen(argv[1],"rb");

  unsigned char c;

  while (fread(&c,1,1,f))
  {
    int isGood = c >= ' ' && c < 128;

    switch (state)
    {
      case 0:
        if (c == '^')
          state = 1;
        else
        {
          totalChars = 0;
          badChars = 0;
        }

        break;

      case 1:
        if (isGood)
          buffer[totalChars] = c;
        else
          badChars++;

        if (badChars > NICK_MAX_BAD_CHARS || totalChars > NICK_MAX_LEN)
          state = 0;
        else if (c == ':' && 
          (
            (buffer[totalChars - 1] == '7' && buffer[totalChars - 2] == '^') ||
            (buffer[totalChars - 1] == 'u' && buffer[totalChars - 2] == 'o') ||
            (buffer[0] == '3' && buffer[1] == 'Y' && buffer[2] == 'o')
          ))
          state = totalChars >= NICK_MIN_LEN && totalChars <= NICK_MAX_LEN ? 2 : 0;

        totalChars++;
        break;

      case 2:
        if (totalChars <= BUFFER_SIZE)
          buffer[totalChars] = c;

        totalChars++;

        state = c == ' ' ? 3 : 0;
        break;

      case 3:
        if (isGood)
          buffer[totalChars] = c;
        else
          badChars++;

        if (badChars > TOTAL_MAX_BAD_CHARS || totalChars >= BUFFER_SIZE - 2)
        {
          buffer[totalChars + 1] = 0;

          if (totalChars >= TOTAL_MIN_LEN)
            printf("%s\n",buffer);

          state = 0;
        }

        totalChars++;
        break;
        
      default: break;
    }
  }

  return 0;
}
