/*
  Obfuscates text information, may confuse some idiots.

  CC0 1.0
*/

#include <stdio.h>

int main()
{
  int p = 3; // CHOOSE PASSWORD HERE

  int q = 0;

  p *= p;

  while (1)
  {
    int c = getchar();

    if (c == EOF)
      break;

    if (c >= ' ' && c < 128)
    {
      c -= ' ';

      switch ((p >> (q % 8)) & 0x03)
      {
        case 0: c = (127 - ' ' - c); break;
        case 1: c = ((c & 0xfa) | ((c & 0x01) << 2) | ((c & 0x04) >> 2)); break;
        case 2: c = (c & 0xfb) | ((~c) & 0x04); break;
        case 3: c = (c & 0xf0) | ((~c) & 0x0f); break;
        default: break;
      }

      c += ' ';
    }

    printf("%c",c);

    q++;
  }
}
